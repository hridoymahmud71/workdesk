<?php

/*
|--------------------------------------------------------------------------
| Test Routes
|--------------------------------------------------------------------------
|
| For testing only
|
*/



Route::get('/test_404', 'HomeController@test_404')->name('test_404');
Route::get('/test_500', 'HomeController@test_500')->name('test_500');
