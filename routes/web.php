<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//dummy routes

Route::get('/', 'Front\HomepageController@index')->name('front_homepage');
//Auth::routes();

//auth
Route::match(array('GET','POST'),'login', 'Front\AuthController@login');
Route::match(array('GET','POST'),'register', 'Front\AuthController@register');

$dummy_routes = [
                'account','my_task','projects','invite_friends',
                'about','services','career','blog',
                'contact','help','terms','privacy'
                ];
$imploded_dummy_routes = implode('|',$dummy_routes);

Route::get('/{url}', function ($url) {

    return Redirect::to('/');

})->where(['url' => "($imploded_dummy_routes)" ]);

//searches
//Route::get('/project_search', 'Front\Project\SearchController@index');
Route::get('/freelancer_search', 'Front\Freelancer\SearchController@index');

//resources
Route::resource('project', 'Front\Project\ViewController');
Route::resource('freelancer', 'Front\Freelancer\ViewController');

Route::get('/project{id}', 'Front\Project\ViewController@show');


