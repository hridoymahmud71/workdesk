<?php
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
| This file is created to store admin routes
|
*/

Route::get('/admin', 'Admin\DefaultController@index')->name('admin_default');

//pre-auth
Route::get('/admin/login', 'Admin\AuthController@login')->name('admin_login');
Route::get('/admin/forgot_password', 'Admin\AuthController@forgot_password')->name('admin_forgot_password');
Route::get('/admin/forgot_password/sent', 'Admin\AuthController@forgot_password_sent')->name('admin_forgot_password_sent');

//post-auth
Route::get('admin/dashboard', 'Admin\DashboardController@index')->name('admin_dashboard');
