<?php

if (!function_exists('DummyFunction')) {

    if (!function_exists('plurality')) {

        function plurality($count)
        {
            $s = $count > 0 ? "s":"";
            $is_or_are = $count > 0 ? "are":"is";

            return compact('s','is_or_are');
        }
    }
}
