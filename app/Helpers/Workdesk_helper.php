<?php
use App\Workdesk;

if (!function_exists('get_workdesk_single_value')) {

    function get_workdesk_single_value($group,$key)
    {
        $value = Workdesk::where([['group',"=", $group],['key',"=", $key]])
                ->select('key', 'value')
                ->value('value');

        return $value;
    }
}

if (!function_exists('application_version')) {

    function application_version()
    {
        $value = get_workdesk_single_value('application','version') ??  "unknown";

        return $value;
    }
}

if (!function_exists('application_version')) {

    function application_version()
    {
        $value = get_workdesk_single_value('application','version') ??  "unknown";

        return $value;
    }
}

if (!function_exists('application_copyright')) {

    function application_copyright()
    {
        $value = get_workdesk_single_value('application','copyright') ??  "@Copyright of Active IT Zone , 2015 - ".date('Y');

        return $value;
    }
}

if (!function_exists('site_name')) {

    function site_name()
    {
        $value = get_workdesk_single_value('site','name') ??  "Workdesk";

        return $value;
    }
}

if (!function_exists('site_description')) {

    function site_description()
    {
        $value = get_workdesk_single_value('site','description') ??  "";

        return $value;
    }
}

if (!function_exists('site_copyright')) {

    function site_copyright()
    {
        $value = get_workdesk_single_value('site','copyright') ??  application_copyright();

        return $value;
    }
}
