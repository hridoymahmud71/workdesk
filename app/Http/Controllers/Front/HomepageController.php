<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class HomepageController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {

        $page_name = __("Home");
        $title = site_name()." - ".$page_name;

        return view('front.pages.homepage.skeleton', compact('title','page_name'));
    }
}
