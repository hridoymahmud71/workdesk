<?php

namespace App\Http\Controllers\Front\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
class SearchController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $page_name = __("Search Projects");
        $title = site_name()." - ".$page_name;

        $params = Input::all();

        //print_r($params);exit;

        return view('front.pages.project.search.skeleton', compact('title','page_name','params'));
    }
}
