<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct()
    {

    }

    public function login(Request $request)
    {
        if(!empty($request->toArray())){
            $this->login_check($request);
            return;
        }

        $page_name = __("Login");
        $title = site_name()." - ".$page_name;

        $page_description = __("Enter your email address and password to access account.");

        return view('front.auth.login', compact('title','page_name','page_description'));

    }

    private function login_check($request)
    {
        $input = $request->except(['_token'])->toArray();
    }

    public function register(Request $request)
    {
        if(!empty($request->toArray())){
            $this->login_check($request);
            return;
        }

        $page_name = __("Register");
        $title = site_name()." - ".$page_name;

        $page_description = __("Enter your email address and password to access account.");

        return view('front.auth.register', compact('title','page_name','page_description'));

    }

    private function register_check($request)
    {
        $input = $request->except(['_token'])->toArray();
    }
}
