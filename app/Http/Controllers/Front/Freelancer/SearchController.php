<?php

namespace App\Http\Controllers\Front\Freelancer;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function __construct()
    {

    }

    public function index()
    {
        $page_name = __("Search freelancer");
        $title = site_name()." - ".$page_name;

        $params = Input::all();

        return view('front.pages.freelancer.search.skeleton', compact('title','page_name','params'));
    }
}
