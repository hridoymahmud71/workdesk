<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct()
    {
        
    }

    public function index()
    {
        //echo "Hello this is admin dashboard";exit;


        $page_name = __("Admin Dashboard");
        $title = site_name()." - ".$page_name;

        $breadcrumb = [];
        $breadcrumb[] = ["anchor"=>route('admin_default'),"name"=>__("Admin")];
        $breadcrumb[] = ["anchor"=>"","name"=>__("Dashboard")];

        return view('back.pages.dashboard', compact('title','page_name','breadcrumb'));
    }
}
