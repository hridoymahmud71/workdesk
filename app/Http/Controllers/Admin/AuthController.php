<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct()
    {

    }

    public function login(Request $request)
    {
        if(!empty($request->toArray())){
            $this->login_check($request);
            return;
        }

        $page_name = __("Admin Login");
        $title = site_name()." - ".$page_name;

        $page_description = __("Enter your email address and password to access account.");

        return view('back.auth.pages.login', compact('title','page_name','page_description'));

    }

    private function login_check($request)
    {
        $input = $request->except(['_token'])->toArray();
    }

    public function forgot_password(Request $request)
    {
        if(!empty($request->toArray())){
            $this->login_check($request);
            return;
        }

        $page_name = __("Password Forgotten");
        $title = site_name()." - ".$page_name;
        $page_description = __("Enter your email address and we will send you an email with instructions to reset your password.");

        return view('back.auth.pages.forgot_password', compact('title','page_name','page_description'));

    }

    private function forgot_password_check($request)
    {
        $input = $request->except(['_token'])->toArray();
    }

    public function forgot_password_sent()
    {
        $page_name = __("Email for password is sent");
        $title = site_name()." - ".$page_name;

        return view('back.auth.pages.forgot_password_sent', compact('title','page_name'));

    }
}
