
<!-- Favicon -->
<link rel="shortcut icon" href="{{ URL::asset('front-v2.5.0/favicon.ico') }}">

<!-- Google Fonts -->
<link href="//fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

<!-- CSS Implementing Plugins -->
<link rel="stylesheet" href="{{ URL::asset('front-v2.5.0/assets/vendor/font-awesome/css/fontawesome-all.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('front-v2.5.0/assets/vendor/animate.css/animate.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('front-v2.5.0/assets/vendor/hs-megamenu/src/hs.megamenu.css') }}">
<link rel="stylesheet" href="{{ URL::asset('front-v2.5.0/assets/vendor/fancybox/jquery.fancybox.css') }}">
<link rel="stylesheet" href="{{ URL::asset('front-v2.5.0/assets/vendor/slick-carousel/slick/slick.css') }}">
<link rel="stylesheet" href="{{ URL::asset('front-v2.5.0/assets/vendor/bootstrap-select/dist/css/bootstrap-select.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('front-v2.5.0/assets/vendor/ion-rangeslider/css/ion.rangeSlider.css') }}">
<!-- CSS Front Template -->
<link rel="stylesheet" href="{{ URL::asset('front-v2.5.0/assets/css/theme.css') }}">
