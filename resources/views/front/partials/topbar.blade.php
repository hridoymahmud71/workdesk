<div class="container u-header__hide-content pt-2">
<div class="d-flex align-items-center">
    <div class="ml-auto">
        <!-- Jump To -->
        <div class="d-inline-block d-sm-none position-relative mr-4">
            <a id="jumpToDropdownInvoker" class="dropdown-nav-link dropdown-toggle d-flex align-items-center" href="javascript:;" role="button"
               aria-controls="jumpToDropdown"
               aria-haspopup="true"
               aria-expanded="false"
               data-unfold-event="hover"
               data-unfold-target="#jumpToDropdown"
               data-unfold-type="css-animation"
               data-unfold-duration="300"
               data-unfold-delay="300"
               data-unfold-hide-on-scroll="true"
               data-unfold-animation-in="slideInUp"
               data-unfold-animation-out="fadeOut">
                Jump to
            </a>

            <div id="jumpToDropdown" class="dropdown-menu dropdown-unfold" aria-labelledby="jumpToDropdownInvoker">
                <a class="dropdown-item" href="'contact'">+1 062 109 92 22</a>
                <a class="dropdown-item" href="'resume'">Upload your resume</a>
            </div>
        </div>
        <!-- End Jump To -->

        <!-- Sign In -->
        <div class="d-inline-block d-sm-none">
            <a class="small font-weight-medium text-uppercase" href="login">Sign in</a>
        </div>
        <!-- End Sign In -->

        <!-- Links -->
        <div class="d-none d-sm-inline-block ml-sm-auto">
            <ul class="list-inline mb-0">
                <li class="list-inline-item">
                    <a class="u-header__navbar-link" href="#">
                        <span class="far fa-file-alt mr-1"></span>
                        Upload your resume
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="u-header__navbar-link" href="{{URL::to('login')}}">
                        <span class="fas fa-user-circle mr-1"></span>
                        Sign in
                    </a>
                </li>
            </ul>
        </div>
        <!-- End Links -->
    </div>
</div>
</div>
