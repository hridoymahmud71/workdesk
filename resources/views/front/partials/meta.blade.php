<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="{{ $meta_description??"This a job portal" }}">
<meta name="keywords" content="{{ $meta_keyword??"job,freelancer,search" }}">
<meta name="author" content="{{ $metha_author->author??"Active IT Zone" }}">
<meta name="sitemap_link" content="{{ $meta_sitemap_link??"default" }}">

<meta name="developed by" content="Mahmudur Rahman, Software Engineer, Active IT Zone">
<meta name="developed by" content="Dev 2, Software Engineer, Active IT Zone">
<meta name="developed by" content="Dev 3, Software Engineer, Active IT Zone">
