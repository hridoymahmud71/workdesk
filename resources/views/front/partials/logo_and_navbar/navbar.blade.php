<div id="navBar" class="collapse navbar-collapse u-header__navbar-collapse">
    <ul class="navbar-nav u-header__navbar-nav">
        <!-- Link -->
        <li class="nav-item u-header__nav-item">
            <a class="nav-link u-header__nav-link" href="find_jobs">Find jobs</a>
        </li>
        <!-- End Link -->

        <!-- Resources -->
        <li class="nav-item hs-has-mega-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut"
            data-max-width="760px"
            data-position="right">
            <a id="resourcesMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">Resources</a>

            <!-- Resources - Submenu -->
            <div class="hs-mega-menu u-header__sub-menu" aria-labelledby="resourcesMegaMenu">
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <!-- Promo Item -->
                        <div class="u-header__promo-item">
                            <a class="u-header__promo-link" href="job-description.html">
                                <div class="media">
                                    <img class="js-svg-injector u-header__promo-icon" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-19.svg') }}" alt="SVG">
                                    <div class="media-body">
                                        <span class="u-header__promo-title mb-1">Job description</span>
                                        <small class="u-header__promo-text">Find out more about jobs.</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- End Promo Item -->

                        <!-- Promo Item -->
                        <div class="u-header__promo-item">
                            <a class="u-header__promo-link" href="employee-profile.html">
                                <div class="media">
                                    <img class="js-svg-injector u-header__promo-icon" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-48.svg') }}" alt="SVG">
                                    <div class="media-body">
                                        <span class="u-header__promo-title mb-1">Employee profile</span>
                                        <small class="u-header__promo-text">Search and compare employees.</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- End Promo Item -->

                        <!-- Promo Item -->
                        <div class="u-header__promo-item">
                            <a class="u-header__promo-link" href="company-profile.html">
                                <div class="media">
                                    <img class="js-svg-injector u-header__promo-icon" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-13.svg') }}" alt="SVG">
                                    <div class="media-body">
                                        <span class="u-header__promo-title mb-1">Company profile</span>
                                        <small class="u-header__promo-text">Find great places to work.</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- End Promo Item -->
                    </div>

                    <!-- Promo -->
                    <div class="col-md-6 u-header__promo">
                        <div class="u-header__promo-inner">
                            <div class="row">
                                <div class="col-sm-6">
                                    <!-- Sub Nav Group -->
                                    <span class="u-header__sub-menu-title">Jobs</span>
                                    <ul class="u-header__sub-menu-nav-group mb-3">
                                        <li><a class="nav-link u-header__sub-menu-nav-link" href="jobs-grid.html">Grid</a></li>
                                        <li><a class="nav-link u-header__sub-menu-nav-link" href="jobs-grid-side-filter.html">Grid side filter</a></li>
                                        <li><a class="nav-link u-header__sub-menu-nav-link" href="jobs-list.html">Listing</a></li>
                                        <li><a class="nav-link u-header__sub-menu-nav-link" href="jobs-list-side-filter.html">Listing side filter</a></li>
                                    </ul>
                                    <!-- End Sub Nav Group -->
                                </div>

                                <div class="col-sm-6">
                                    <!-- Sub Nav Group -->
                                    <span class="u-header__sub-menu-title">Freelancers</span>
                                    <ul class="u-header__sub-menu-nav-group">
                                        <li><a class="nav-link u-header__sub-menu-nav-link" href="freelancers-grid.html">Grid</a></li>
                                        <li><a class="nav-link u-header__sub-menu-nav-link" href="freelancers-grid-side-filter.html">Grid side filter</a></li>
                                        <li><a class="nav-link u-header__sub-menu-nav-link" href="freelancers-list.html">Listing</a></li>
                                        <li><a class="nav-link u-header__sub-menu-nav-link" href="freelancers-list-side-filter.html">Listing side filter</a></li>
                                    </ul>
                                    <!-- End Sub Nav Group -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Promo -->
                </div>
            </div>
            <!-- End Resources - Submenu -->
        </li>
        <!-- End Resources -->

        <!-- Link -->
        <li class="nav-item u-header__nav-item">
            <a class="nav-link u-header__nav-link" href="companies.html">Company reviews</a>
        </li>
        <!-- End Link -->

        <!-- Demos -->
        <li class="nav-item hs-has-mega-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut"
            data-max-width="600px"
            data-position="right">
            <a id="demosMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">Demos</a>

            <!-- Demos - Mega Menu -->
            <div class="hs-mega-menu w-100 u-header__sub-menu" aria-labelledby="demosMegaMenu">
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <!-- Promo Item -->
                        <div class="u-header__promo-item">
                            <a class="u-header__promo-link" href="../home/index.html">
                                <div class="media align-items-center">
                                    <img class="js-svg-injector u-header__promo-icon" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-21.svg') }}" alt="SVG">
                                    <div class="media-body">
                                        <span class="u-header__promo-title">Main demo</span>
                                        <small class="u-header__promo-text">Main demo pages.</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- End Promo Item -->

                        <!-- Promo Item -->
                        <div class="u-header__promo-item">
                            <a class="u-header__promo-link" href="../house/index.html">
                                <div class="media align-items-center">
                                    <img class="js-svg-injector u-header__promo-icon" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-13.svg') }}" alt="SVG">
                                    <div class="media-body">
                                        <span class="u-header__promo-title">House</span>
                                        <small class="u-header__promo-text">Real estate demo.</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- End Promo Item -->

                        <!-- Promo Item -->
                        <div class="u-header__promo-item">
                            <a class="u-header__promo-link" href="../crypto/index.html">
                                <div class="media align-items-center">
                                    <img class="js-svg-injector u-header__promo-icon" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-5.svg') }}" alt="SVG">
                                    <div class="media-body">
                                        <span class="u-header__promo-title">Crypto landing</span>
                                        <small class="u-header__promo-text">Cryptocurrency demo.</small>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <!-- End Promo Item -->
                    </div>

                    <!-- Promo -->
                    <div class="col-md-6 u-header__promo">
                        <a class="d-block u-header__promo-inner" href="#">
                            <div class="position-relative">
                                <img class="img-fluid rounded mb-3" src="{{ URL::asset('front-v2.5.0/assets/img/380x227/img6.jpg') }}" alt="Image Description">
                                <span class="badge badge-success badge-pill badge-pos shadow-sm mt-3">New</span>
                            </div>
                            <span class="text-secondary font-size-1">Front makes you look at things from a different perspectives.</span>
                        </a>
                    </div>
                    <!-- End Promo -->
                </div>
            </div>
            <!-- End Demos - Mega Menu -->
        </li>
        <!-- End Demos -->

        <!-- Docs -->
        <li class="nav-item hs-has-mega-menu u-header__nav-item"
            data-event="hover"
            data-animation-in="slideInUp"
            data-animation-out="fadeOut"
            data-max-width="260px"
            data-position="right">
            <a id="docsMegaMenu" class="nav-link u-header__nav-link u-header__nav-link-toggle" href="javascript:;" aria-haspopup="true" aria-expanded="false">Docs</a>

            <!-- Docs - Submenu -->
            <div class="hs-mega-menu u-header__sub-menu" aria-labelledby="docsMegaMenu" style="min-width: 330px;">
                <!-- Promo Item -->
                <div class="u-header__promo-item">
                    <a class="u-header__promo-link" href="../../documentation/index.html">
                        <div class="media align-items-center">
                            <img class="js-svg-injector u-header__promo-icon" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-2.svg') }}" alt="SVG">
                            <div class="media-body">
                          <span class="u-header__promo-title">
                            Documentation
                            <span class="badge badge-primary badge-pill ml-1">v2.3</span>
                          </span>
                                <small class="u-header__promo-text">Development guides</small>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- End Promo Item -->

                <!-- Promo Item -->
                <div class="u-header__promo-item">
                    <a class="u-header__promo-link" href="../../starter/index.html">
                        <div class="media align-items-center">
                            <img class="js-svg-injector u-header__promo-icon" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-1.svg') }}" alt="SVG">
                            <div class="media-body">
                                <span class="u-header__promo-title">Get started</span>
                                <small class="u-header__promo-text">Components and snippets</small>
                            </div>
                        </div>
                    </a>
                </div>
                <!-- End Promo Item -->

                <div class="u-header__promo-footer">
                    <!-- List -->
                    <div class="row no-gutters">
                        <div class="col-6">
                            <div class="u-header__promo-footer-item">
                                <small class="text-muted d-block">Check what's new</small>
                                <a class="small" href="changelog">
                                    Changelog
                                </a>
                            </div>
                        </div>
                        <div class="col-6 u-header__promo-footer-ver-divider">
                            <div class="u-header__promo-footer-item">
                                <small class="text-muted d-block">Have a question?</small>
                                <a class="small" href="contact">
                                    Contact us
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- End List -->
                </div>
            </div>
            <!-- End Docs - Submenu -->
        </li>
        <!-- End Docs -->

        <!-- Button -->
        <li class="nav-item u-header__nav-last-item">
            <a class="btn btn-sm btn-primary transition-3d-hover" href="https://themes.getbootstrap.com/product/front-multipurpose-responsive-template/" target="_blank">
                Post Jobs
            </a>
        </li>
        <!-- End Button -->
    </ul>
</div>
