<div id="logoAndNav" class="container">
    <!-- Nav -->
    <nav class="js-mega-menu navbar navbar-expand-md u-header__navbar u-header__navbar--no-space">
        <!-- Logo -->
        @include('front.partials.logo_and_navbar.logo')
        <!-- End Logo -->

        <!-- Responsive Toggle Button -->
        <button type="button" class="navbar-toggler btn u-hamburger"
                aria-label="Toggle navigation"
                aria-expanded="false"
                aria-controls="navBar"
                data-toggle="collapse"
                data-target="#navBar">
            <span id="hamburgerTrigger" class="u-hamburger__box">
              <span class="u-hamburger__inner"></span>
            </span>
        </button>
        <!-- End Responsive Toggle Button -->

        <!-- Navigation -->
        @include('front.partials.logo_and_navbar.navbar')
        <!-- End Navigation -->
    </nav>
    <!-- End Nav -->
</div>
