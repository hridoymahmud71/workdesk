<div class="row justify-content-lg-between align-items-md-center space-2">
    <div class="col-md-6 col-lg-5 mb-5 mb-md-0">
        <h3 class="text-white font-weight-medium mb-1">Ready to get started?</h3>
        <p class="text-white-70 mb-0">Create your account and find your dream job.</p>
    </div>

    <div class="col-md-6 col-lg-5 text-md-right">
        <a class="btn btn-success btn-wide transition-3d-hover mb-1 mb-sm-0 mr-sm-1" href="sign_up">Sign Up</a>
        <a class="btn text-primary btn-white btn-wide transition-3d-hover mb-1 mb-sm-0" href="#">Learn More</a>
    </div>
</div>
