<div class="position-relative">
    <a id="footerCountryInvoker" class="dropdown-nav-link text-white-70" href="javascript:;" role="button"
       aria-controls="footer-country"
       aria-haspopup="true"
       aria-expanded="false"
       data-toggle="dropdown"
       data-unfold-event="click"
       data-unfold-target="#footer-country"
       data-unfold-type="css-animation"
       data-unfold-duration="300"
       data-unfold-delay="300"
       data-unfold-hide-on-scroll="false"
       data-unfold-animation-in="slideInUp"
       data-unfold-animation-out="fadeOut">
        <img class="dropdown-item-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/us.svg') }}" alt="United States Flag">
        <span class="font-weight-semi-bold">United States</span>
    </a>

    <div id="footer-country" class="dropdown-menu dropdown-unfold dropdown-card dropdown-menu-bottom" aria-labelledby="footerCountryInvoker">
        <div class="card">
            <!-- Body -->
            <div class="card-body list-group list-group-flush list-group-borderless p-5">
                <h4 class="h6 font-weight-semi-bold">Front available in</h4>

                <div class="row">
                    <div class="col-6">
                        <!-- List Group -->
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/au.svg') }}" alt="Australia Flag">
                            Australia
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/at.svg') }}" alt="Austria Flag">
                            Austria
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/be.svg') }}" alt="Belgium Flag">
                            Belgium
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/ca.svg') }}" alt="Canada Flag">
                            Canada
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/dk.svg') }}" alt="Denmark Flag">
                            Denmark
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/fi.svg"') }}" alt="Finland Flag">
                            Finland
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/fr.svg') }}" alt="France Flag">
                            France
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/de.svg') }}" alt="Germany Flag">
                            Germany
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/nl.svg') }}" alt="Netherlands Flag">
                            Netherlands
                        </a>
                        <!-- End List Group -->
                    </div>

                    <div class="col-6">
                        <!-- List Group -->
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/nz.svg') }}" alt="New Zealand Flag">
                            New Zealand
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/no.svg') }}" alt="Norway Flag">
                            Norway
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/pt.svg') }}" alt="PortugalPREVIEW Flag">
                            Portugal
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/sg.svg') }}" alt="Singapore Flag">
                            Singapore
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/es.svg') }}" alt="Spain Flag">
                            Spain
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/se.svg') }}" alt="Sweden Flag">
                            Sweden
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/ch.svg') }}" alt="Switzerland Flag">
                            Switzerland
                        </a>
                        <a class="list-group-item list-group-item-action" href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/gb.svg') }}" alt="United Kingdom Flag">
                            UK
                        </a>
                        <a class="list-group-item list-group-item-action active " href="#">
                            <img class="list-group-icon" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/4x3/us.svg') }}" alt="United States Flag">
                            US
                        </a>
                        <!-- End List Group -->
                    </div>
                </div>
            </div>
            <!-- End Body -->

            <!-- Footer -->
            <a class="card-footer card-bg-light p-5" href="#">
                <span class="d-block text-muted mb-1">More countries coming soon.</span>
                <small class="d-block">Signup to get notified <span class="fas fa-arrow-right small"></span></small>
            </a>
            <!-- End Footer -->
        </div>
    </div>
</div>
