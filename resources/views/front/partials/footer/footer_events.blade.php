<ul class="list-unstyled mt-3 mb-0">
    <li class="mb-3">
        <a class="d-block bg-primary text-white shadow-primary-lg rounded-pill p-1" href="#">
            <div class="media align-items-center">
                <span class="btn btn-xs btn-soft-white btn-pill mr-2">23/05</span>
                <div class="media-body">
                    <span class="font-size-1">Technical Hiring</span>
                </div>
            </div>
        </a>
    </li>
    <li class="mb-3">
        <a class="d-block bg-primary text-white shadow-primary-lg rounded-pill p-1" href="#">
            <div class="media align-items-center">
                <span class="btn btn-xs btn-soft-white btn-pill mr-2">01/06</span>
                <div class="media-body">
                    <span class="font-size-1">Networking Event</span>
                </div>
            </div>
        </a>
    </li>
</ul>
