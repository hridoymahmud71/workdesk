<footer class="gradient-half-primary-v4">
    <div class="container">
        <!-- CTA -->
        @include('front.partials.footer.footer_top')
        <!-- End CTA -->

        <hr class="opacity-md my-0">

        <div class="row justify-content-md-between space-2">
            <div class="col-6 col-sm-4 col-lg-2 order-lg-2 mb-7 mb-lg-0">
                <h4 class="h6 text-white">Account</h4>

                <!-- List Group -->
                <ul class="list-group list-group-transparent list-group-white list-group-flush list-group-borderless mb-0">
                    <li><a class="list-group-item list-group-item-action" href="account">Account</a></li>
                    <li><a class="list-group-item list-group-item-action" href="my_task">My tasks</a></li>
                    <li><a class="list-group-item list-group-item-action" href="projects">Projects</a></li>
                    <li><a class="list-group-item list-group-item-action" href="invite_friends">Invite friends</a></li>
                </ul>
                <!-- End List Group -->
            </div>

            <div class="col-6 col-sm-4 col-lg-2 order-lg-3 mb-7 mb-lg-0">
                <h4 class="h6 text-white">Company</h4>

                <!-- List Group -->
                <ul class="list-group list-group-transparent list-group-white list-group-flush list-group-borderless mb-0">
                    <li><a class="list-group-item list-group-item-action" href="about">About</a></li>
                    <li><a class="list-group-item list-group-item-action" href="services">Services</a></li>
                    <li><a class="list-group-item list-group-item-action" href="career">Careers</a></li>
                    <li><a class="list-group-item list-group-item-action" href="blog">Blog</a></li>
                </ul>
                <!-- End List Group -->
            </div>

            <div class="col-sm-4 col-lg-2 order-lg-4 mb-7 mb-lg-0">
                <h4 class="h6 text-white">Resources</h4>

                <!-- List Group -->
                <ul class="list-group list-group-transparent list-group-white list-group-flush list-group-borderless mb-0">
                    <li><a class="list-group-item list-group-item-action" href="contact">Contacts</a></li>
                    <li><a class="list-group-item list-group-item-action" href="help">Help</a></li>
                    <li><a class="list-group-item list-group-item-action" href="terms">Terms</a></li>
                    <li><a class="list-group-item list-group-item-action" href="privacy">Privacy</a></li>
                </ul>
                <!-- End List Group -->
            </div>

            <div class="col-sm-6 col-md-5 col-lg-3 order-lg-5 mb-6 mb-sm-0">
                <h4 class="h6 text-white">Events</h4>

                <!-- Events Group -->
                @include('front.partials.footer.footer_events')
                <!-- End Events -->
            </div>

            <div class="col-sm-6 col-md-5 col-lg-3 order-lg-1">
                <div class="d-inline-flex align-self-start flex-column h-100">
                    <!-- Logo -->
                    @include('front.partials.footer.footer_logo')
                    <!-- End Logo -->

                    <!-- Country -->
                    @include('front.partials.footer.footer_country')
                    <!-- End Country -->

                    <!-- Copyright -->
                    <p class="small text-white-70 mt-lg-auto mb-0">{{site_copyright()}}</p>
                    <!-- End Copyright -->
                </div>
            </div>
        </div>
    </div>
</footer>
