<!-- JS Global Compulsory -->
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/jquery-migrate/dist/jquery-migrate.min.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/popper.js/dist/umd/popper.min.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/bootstrap/bootstrap.min.js') }}"></script>

<!-- JS Implementing Plugins -->
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/hs-megamenu/src/hs.megamenu.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/svg-injector/dist/svg-injector.min.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/jquery-validation/dist/jquery.validate.min.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/fancybox/jquery.fancybox.min.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/slick-carousel/slick/slick.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/vendor/bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>

<script src="{{ URL::asset('front-v2.5.0/assets/vendor/ion-rangeslider/js/ion.rangeSlider.min.js') }}"></script>
<!-- JS Front -->
<script src="{{ URL::asset('front-v2.5.0/assets/js/hs.core.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.header.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.unfold.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.focus-state.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.validation.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.fancybox.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.slick-carousel.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.show-animation.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.selectpicker.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.range-slider.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.svg-injector.js') }}"></script>
<script src="{{ URL::asset('front-v2.5.0/assets/js/components/hs.go-to.js') }}"></script>

