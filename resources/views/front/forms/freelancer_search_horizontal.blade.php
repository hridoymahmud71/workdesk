<form class="js-validate" action="{{URL::to('/freelancer_search')}}" >
    <div class="row">
        <div class="col-lg-5 mb-4 mb-lg-0">
            <!-- Input -->
            <label class="d-block">
                <span class="h4 d-block text-dark font-weight-semi-bold mb-0">Who</span>
                <small class="d-block text-secondary">Name, Email</small>
            </label>
            <div class="js-focus-state">
                <div class="input-group">
                    <input type="text" name="name" value="{{Input::has('name')?Input::get('name'):''}}" class="form-control" placeholder="Keyword or title" aria-label="Keyword or title" aria-describedby="keywordInputAddon">
                    <div class="input-group-append">
                                    <span class="input-group-text">
                                      <span class="fas fa-search" id="keywordInputAddon"></span>
                                    </span>
                    </div>
                </div>
            </div>
            <!-- End Input -->
        </div>

        <div class="col-lg-5 mb-4 mb-lg-0">
            <!-- Input -->
            <label class="d-block">
                <span class="h4 d-block text-dark font-weight-semi-bold mb-0">where</span>
                <small class="d-block text-secondary">Location</small>
            </label>
            <div class="js-focus-state">
                <div class="input-group">
                    <input type="text" name="location" value="{{Input::has('location')?Input::get('location'):''}}" class="form-control" placeholder="City, state, or zip" aria-label="City, state, or zip" aria-describedby="locationInputAddon">
                    <div class="input-group-append">
                        <span class="input-group-text">
                          <span class="fas fa-map-marker-alt" id="locationInputAddon"></span>
                        </span>
                    </div>
                </div>
            </div>
            <!-- End Input -->
        </div>

        <div class="col-lg-2 align-self-lg-end">
            <button type="submit" class="btn btn-block btn-primary transition-3d-hover">{{__('Find Freelancers')}}</button>
        </div>


    </div>
    <!-- End Checkbox -->
</form>
