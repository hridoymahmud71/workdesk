<form class="js-validate" action="{{URL::to('/project_search')}}">
    <div class="row">
        <div class="col-lg-5 mb-4 mb-lg-0">
            <!-- Input -->
            <label class="d-block">
                <span class="h4 d-block text-dark font-weight-semi-bold mb-0">what</span>
                <small class="d-block text-secondary">name,idea</small>
            </label>
            <div class="js-focus-state">
                <div class="input-group">
                    <input type="text" name="name" value="{{Input::has('name')?Input::get('name'):''}}" class="form-control" placeholder="Keyword or title" aria-label="Keyword or title" aria-describedby="keywordInputAddon">
                    <div class="input-group-append">
                                    <span class="input-group-text">
                                      <span class="fas fa-search" id="keywordInputAddon"></span>
                                    </span>
                    </div>
                </div>
            </div>
            <!-- End Input -->
        </div>

        <div class="col-lg-5 mb-4 mb-lg-0">
            <!-- Input -->
            <label class="d-block">
                <span class="h4 d-block text-dark font-weight-semi-bold mb-0">Type</span>
                <small class="d-block text-secondary">what type?</small>
            </label>
            <div class="js-focus-state">
                <div class="input-group">
                    {{--<input type="text" name="type" value="{{Input::has('type')?Input::get('type'):''}}" class="form-control" placeholder="type" aria-label="City, state, or zip" aria-describedby="locationInputAddon">--}}
                    <select name="type" class="form-control" id="">

                        @php
                            $types =
                            [
                                0=>['key'=>"tp1",'name'=>"Type 1"],
                                1=>['key'=>"tp2",'name'=>"Type 2"],
                                2=>['key'=>"tp3",'name'=>"Type 3"],
                                3=>['key'=>"tp4",'name'=>"Type 4"],
                            ];
                        @endphp

                        @if(!empty($types))
                            <option value="">{{__('Select a type')}}</option>
                            @foreach($types as $k=>$type )
                                <option value="{{$type['key']}}">{{$type['name']}}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
            </div>
            <!-- End Input -->
        </div>

        <div class="col-lg-2 align-self-lg-end">
            <button type="submit" class="btn btn-block btn-primary transition-3d-hover">{{__('Find Projects')}}</button>
        </div>



    </div>
    <!-- End Checkbox -->
</form>
