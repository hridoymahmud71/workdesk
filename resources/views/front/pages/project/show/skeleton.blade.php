@extends('front.master')
@section('content')
    <div class="container space-2">
        <div class="row">
            <div class="col-lg-4 mb-9 mb-lg-0">
                <!-- Sidebar Info -->
                @include('front.pages.project.show.segments.client_sidebar')
                <!-- End Sidebar Info -->
            </div>

            <div class="col-lg-8">
                <div class="pl-lg-4">
                    <div class="mb-3">
                        <h4 class="h5 mb-0">About project</h4>
                    </div>

                    <!-- Gallery -->
                    @include('front.pages.project.show.segments.project_gallery')
                    <!-- End Gallery -->

                    <div class="border-bottom pb-5 mb-5">
                        <p>Slack is a cloud-based set of proprietary team collaboration tools and services, founded by Stewart Butterfield. Slack began as an internal tool used by his company, Tiny Speck, in the development of Glitch, a now defunct online game. The name is an acronym for "Searchable Log of All Conversation and Knowledge".</p>

                        <p>When your team needs to kick off a project, hire a new employee, deploy some code, review a sales contract, finalize next year's budget, measure an A/B test, plan your next office opening, and more, Slack has you covered.</p>
                    </div>

                    <div class="border-bottom pb-5 mb-5">
                        <!-- Title -->
                        <div class="mb-4">
                            <h2 class="h5">Pledges &amp; Certifications</h2>
                        </div>
                        <!-- End Title -->

                        <div class="row">
                            <div class="col-sm-6 mb-5">
                                <!-- Icon Block -->
                                <div class="media">
                                    <figure id="icon21" class="svg-preloader ie-height-56 w-100 max-width-8 mr-4">
                                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/illustrations/medal.svg') }}" alt="SVG"
                                             data-parent="#icon21">
                                    </figure>
                                    <div class="media-body">
                                        <h3 class="h6 mb-1">Veteran Hiring Commitment</h3>
                                        <p>Committed to helping America's military veterans find work.</p>
                                    </div>
                                </div>
                                <!-- End Icon Block -->
                            </div>

                            <div class="col-sm-6 mb-5">
                                <!-- Icon Block -->
                                <div class="media">
                                    <figure id="icon25" class="svg-preloader ie-height-56 w-100 max-width-8 mr-4">
                                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/illustrations/social-responsibility.svg') }}" alt="SVG"
                                             data-parent="#icon25">
                                    </figure>
                                    <div class="media-body">
                                        <h4 class="h6 mb-1">Social Responsibility Pledge</h4>
                                        <p>Formal programs or foundation to give back to communities.</p>
                                    </div>
                                </div>
                                <!-- End Icon Block -->
                            </div>

                            <div class="w-100"></div>

                            <div class="col-sm-6 mb-5 mb-sm-0">
                                <!-- Icon Block -->
                                <div class="media">
                                    <figure id="icon7" class="svg-preloader ie-height-56 w-100 max-width-8 mr-4">
                                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/illustrations/diversity.svg') }}" alt="SVG"
                                             data-parent="#icon7">
                                    </figure>
                                    <div class="media-body">
                                        <h4 class="h6 mb-1">Diversity Commitment</h4>
                                        <p>Has programs that support a diverse and inclusive workforce.</p>
                                    </div>
                                </div>
                                <!-- End Icon Block -->
                            </div>

                            <div class="col-sm-6">
                                <!-- Icon Block -->
                                <div class="media">
                                    <figure id="icon29" class="svg-preloader ie-height-56 w-100 max-width-8 mr-4">
                                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/illustrations/growth.svg') }}" alt="SVG"
                                             data-parent="#icon29">
                                    </figure>
                                    <div class="media-body">
                                        <h4 class="h6 mb-1">Pledge to Thrive</h4>
                                        <p>Taking steps to prioritize employee well-being.</p>
                                    </div>
                                </div>
                                <!-- End Icon Block -->
                            </div>
                        </div>
                    </div>

                    @include('front.pages.project.show.segments.project_biddings')


                </div>
            </div>
        </div>
    </div>
@endsection
