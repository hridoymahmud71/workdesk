<div class="d-sm-flex justify-content-sm-between align-items-sm-center border-bottom pb-5 mb-5">
    <div class="d-flex align-items-center mb-2 mb-sm-0">
        <h4 class="h5 mb-0">Biddings</h4>

        <!-- Review -->
        <div class="text-warning ml-3">
            <small class="fas fa-star"></small>
            <span class="font-weight-semi-bold align-middle">4.7</span>
            <span class="text-muted align-middle">(39 biddings)</span>
        </div>
        <!-- End Review -->
    </div>

    <!-- Select -->
    <select class="js-select selectpicker dropdown-select"
            data-width="fit"
            data-style="btn-soft-secondary btn-xs">
        <option value="showCategoryByFilterSelect1" selected>Newest first</option>
        <option value="showCategoryByFilterSelect2">Highest bid</option>
        <option value="showCategoryByFilterSelect3">Lowest bid</option>
    </select>
    <!-- End Select -->
</div>

<div class="border-bottom pb-5 mb-5">
    <div class="row">
        <!-- Review -->
        <div class="col-sm-4 mb-3 mb-sm-0">
            <div class="text-warning">
                <small class="fas fa-star"></small>
                <span class="font-weight-semi-bold align-middle">4.6</span>
            </div>
            <h5 class="small text-secondary mb-0">Life balance</h5>
        </div>
        <!-- End Review -->

        <!-- Review -->
        <div class="col-sm-4 mb-3 mb-sm-0">
            <div class="text-warning">
                <small class="fas fa-star"></small>
                <span class="font-weight-semi-bold align-middle">4.8</span>
            </div>
            <h5 class="small text-secondary mb-0">Job security</h5>
        </div>
        <!-- End Review -->

        <!-- Review -->
        <div class="col-sm-4">
            <div class="text-warning">
                <small class="fas fa-star"></small>
                <span class="font-weight-semi-bold align-middle">5.0</span>
            </div>
            <h5 class="small text-secondary mb-0">Management</h5>
        </div>
        <!-- End Review -->
    </div>
</div>

<div class="border-bottom pb-5 mb-5">
    <!-- Testimonial -->
    <div class="mb-4">
        <h4 class="h6">Best company I've worked for so far</h4>
        <p>Most people genuinely care about the product and what they are working on. Many projects are bottom up instead of top down, it's a great place to learn and grow.</p>
    </div>

    <div class="media">
        <div class="u-avatar mr-3">
            <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/100x100/img2.jpg') }}" alt="Image Description">
        </div>
        <div class="media-body">
            <h4 class="h6 mb-1">Kristina Jae</h4>
            <!-- Review -->
            <div class="small mb-3">
                    <span class="text-warning">
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                    </span>
                <span class="font-weight-semi-bold ml-2">5.0</span>
                <span class="text-muted">3 weeks ago</span>
            </div>
            <!-- End Review -->
        </div>
    </div>
    <!-- End Testimonial -->
</div>

<div class="border-bottom pb-5 mb-5">
    <!-- Testimonial -->
    <div class="mb-4">
        <h4 class="h6">People operations interview</h4>
        <p>There is constant discussion among all employees about how we ‘do that right thing’. That looks different for everyone, but I appreciate that there is always an open dialogue around the decisions we make across the whole company.</p>
    </div>

    <div class="media">
        <div class="u-avatar mr-3">
            <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/100x100/img11.jpg')}}" alt="Image Description">
        </div>') }}
        <div class="media-body">
            <h4 class="h6 mb-1">Claudia Kane</h4>
            <!-- Review -->
            <div class="small mb-3">
                    <span class="text-warning">
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                    </span>
                <span class="font-weight-semi-bold ml-2">5.0</span>
                <span class="text-muted">2 months ago</span>
            </div>
            <!-- End Review -->
        </div>
    </div>
    <!-- End Testimonial -->
</div>

<div class="border-bottom pb-5 mb-5">
    <!-- Testimonial -->
    <div class="mb-3">
        <h4 class="h6">Friendly colleagues</h4>
        <p>Everyone is very friendly. I feel welcome and informed throughout a working day.</p>
    </div>

    <div class="media">
        <div class="u-avatar mr-3">
            <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/100x100/img9.jpg') }}" alt="Image Description">
        </div>
        <div class="media-body">
            <h4 class="h6 mb-1">Brian Walters</h4>
            <!-- Review -->
            <div class="small mb-3">
                    <span class="text-warning">
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                    </span>
                <span class="font-weight-semi-bold ml-2">5.0</span>
                <span class="text-muted">5 months ago</span>
            </div>
            <!-- End Review -->
        </div>
    </div>
    <!-- End Testimonial -->
</div>

<button type="button" class="btn btn-block btn-soft-primary transition-3d-hover">Load More</button>
