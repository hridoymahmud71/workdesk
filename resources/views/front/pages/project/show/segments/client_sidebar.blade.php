<div class="card shadow-sm p-5 mb-5">
    <div class="border-bottom text-center pb-5 mb-5">
        <!-- Avatar -->
        <div class="mb-3 mx-auto">
            <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img3.jpg') }}" width="110" height="110" alt="Image Description">
        </div>
        <!-- End Avatar -->

        <h1 class="h5">Slack</h1>

        <!-- Review -->
        <div class="mb-2">
                <span class="text-warning">
                  <small class="fas fa-star"></small>
                  <small class="fas fa-star"></small>
                  <small class="fas fa-star"></small>
                  <small class="fas fa-star"></small>
                  <small class="fas fa-star"></small>
                </span>
            <span class="font-weight-semi-bold ml-2">4.91</span>
            <small class="text-muted">(12k+ reviews)</small>
        </div>
        <!-- End Review -->

        <div class="mb-4">
            <a class="font-size-1" href="#">www.slack.com</a>
        </div>

        <div class="mb-2">
            <!-- Buttons -->
            <a class="btn btn-sm btn-soft-success transition-3d-hover mr-1" href="#">Other projects</a>
            <a class="btn btn-sm btn-soft-primary transition-3d-hover" href="#">64 Projects / 4 running</a>
            <!-- End Buttons -->
        </div>

        <a class="text-muted small" href="#">
            <small class="fas fa-flag mr-1"></small>
            Report
        </a>
    </div>

    <div class="border-bottom pb-5 mb-5">
        <div id="SVGCompanyStatsIcon" class="svg-preloader row">
            <div class="col-6 mb-5">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-8.svg') }}" alt="SVG"
                             data-parent="#SVGCompanyStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">San Francisco</span>
                    <span class="d-block text-secondary font-size-1">Headquarters</span>
                </div>
                <!-- End Stats -->
            </div>

            <div class="col-6 mb-5">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-11.svg') }}" alt="SVG"
                             data-parent="#SVGCompanyStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">2014</span>
                    <span class="d-block text-secondary font-size-1">Founded</span>
                </div>
                <!-- End Stats -->
            </div>

            <div class="col-6 mb-5">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-7.svg') }}" alt="SVG"
                             data-parent="#SVGCompanyStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">12,000+</span>
                    <span class="d-block text-secondary font-size-1">Employees</span>
                </div>
                <!-- End Stats -->
            </div>

            <div class="col-6 mb-5">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-13.svg') }}" alt="SVG"
                             data-parent="#SVGCompanyStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">Internet</span>
                    <span class="d-block text-secondary font-size-1">Industry</span>
                </div>
                <!-- End Stats -->
            </div>

            <div class="col-6">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-22.svg') }}" alt="SVG"
                             data-parent="#SVGCompanyStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">Unknown</span>
                    <span class="d-block text-secondary font-size-1">Revenue</span>
                </div>
                <!-- End Stats -->
            </div>

            <div class="col-6">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-34.svg') }}" alt="SVG"
                             data-parent="#SVGCompanyStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">$25 - $45</span>
                    <span class="d-block text-secondary font-size-1">Avg. Salary</span>
                </div>
                <!-- End Stats -->
            </div>
        </div>
    </div>

    <div class="border-bottom pb-5 mb-5">
        <h4 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Linked accounts</h4>

        <!-- Linked Account -->
        <a class="media align-items-center mb-3" href="#">
            <div class="u-sm-avatar mr-3">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img3.jpg') }}" alt="Image Description">
            </div>
            <div class="media-body">
                <h4 class="font-size-1 text-dark mb-0">Slack</h4>
                <small class="d-block text-secondary">slack.com</small>
            </div>
        </a>
        <!-- End Linked Account -->

        <!-- Linked Account -->
        <a class="media align-items-center mb-3" href="#">
            <div class="u-sm-avatar mr-3">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img19.png') }}" alt="Image Description">
            </div>
            <div class="media-body">
                <h4 class="font-size-1 text-dark mb-0">Twitter</h4>
                <small class="d-block text-secondary">twitter.com/slack</small>
            </div>
        </a>
        <!-- End Linked Account -->

        <!-- Linked Account -->
        <a class="media align-items-center" href="#">
            <div class="u-sm-avatar mr-3">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img20.png') }}" alt="Image Description">
            </div>
            <div class="media-body">
                <h4 class="font-size-1 text-dark mb-0">Facebook</h4>
                <small class="d-block text-secondary">facebook.com/slack</small>
            </div>
        </a>
        <!-- End Linked Account -->
    </div>

    <h4 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Job seekers also viewed</h4>

    <!-- Related Companies -->
    <div class="media align-items-center mb-3">
        <div class="u-sm-avatar mr-3">
            <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img17.png') }}" alt="Image Description">
        </div>
        <div class="media-body">
            <h4 class="font-size-1 mb-0">
                <a href="#">Google</a>
            </h4>

            <div class="d-md-flex align-items-md-center">
                <!-- Review -->
                <span class="text-warning small">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                </span>
                <small class="text-muted ml-2">567k+ reviews</small>
                <!-- End Review -->
            </div>
        </div>
    </div>
    <!-- End Related Companies -->

    <!-- Related Companies -->
    <div class="media align-items-center mb-3">
        <div class="u-sm-avatar mr-3">
            <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img20.png') }}" alt="Image Description">
        </div>
        <div class="media-body">
            <h4 class="font-size-1 mb-0">
                <a href="#">Facebook</a>
            </h4>

            <div class="d-md-flex align-items-md-center">
                <!-- Review -->
                <span class="text-warning small">
                    <span class="fas fa-star"></span>
                    <span class="fas fa-star"></span>
                    <span class="fas fa-star"></span>
                    <span class="fas fa-star"></span>
                    <span class="far fa-star"></span>
                  </span>
                <small class="text-muted ml-2">430 reviews</small>
                <!-- End Review -->
            </div>
        </div>
    </div>
    <!-- End Related Companies -->

    <!-- Related Companies -->
    <div class="media align-items-center mb-3">
        <div class="u-sm-avatar mr-3">
            <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img13.png') }}" alt="Image Description">
        </div>
        <div class="media-body">
            <h4 class="font-size-1 mb-0">
                <a href="#">Asana</a>
            </h4>

            <div class="d-md-flex align-items-md-center">
                <!-- Review -->
                <span class="text-warning small">
                    <span class="fas fa-star"></span>
                    <span class="fas fa-star"></span>
                    <span class="fas fa-star"></span>
                    <span class="far fa-star"></span>
                    <span class="far fa-star"></span>
                  </span>
                <small class="text-muted ml-2">5k+ reviews</small>
                <!-- End Review -->
            </div>
        </div>
    </div>
    <!-- End Related Companies -->

    <!-- View More - Collapse -->
    <div class="collapse" id="collapseRelatedCompanies">
        <!-- Related Companies -->
        <div class="media align-items-center mb-3">
            <div class="u-sm-avatar mr-3">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img21.png') }}" alt="Image Description">
            </div>
            <div class="media-body">
                <h4 class="font-size-1 mb-0">
                    <a href="#">Dribbble</a>
                </h4>

                <div class="d-md-flex align-items-md-center">
                    <!-- Review -->
                    <span class="text-warning small">
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                    </span>
                    <small class="text-muted ml-2">15k+ reviews</small>
                    <!-- End Review -->
                </div>
            </div>
        </div>
        <!-- End Related Companies -->

        <!-- Related Companies -->
        <div class="media align-items-center mb-3">
            <div class="u-sm-avatar mr-3">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img15.png') }}" alt="Image Description">
            </div>
            <div class="media-body">
                <h4 class="font-size-1 mb-0">
                    <a href="#">Spotify</a>
                </h4>

                <div class="d-md-flex align-items-md-center">
                    <!-- Review -->
                    <span class="text-warning small">
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                      <span class="fas fa-star"></span>
                    </span>
                    <small class="text-muted ml-2">987 reviews</small>
                    <!-- End Review -->
                </div>
            </div>
        </div>
        <!-- End Related Companies -->
    </div>
    <!-- End View More - Collapse -->

    <!-- View More - Link -->
    <a class="link link-collapse small font-size-1" data-toggle="collapse" href="#collapseRelatedCompanies" role="button" aria-expanded="false" aria-controls="collapseRelatedCompanies">
        <span class="link-collapse__default">View more</span>
        <span class="link-collapse__active">View less</span>
        <span class="link__icon ml-1">
                <span class="link__icon-inner">+</span>
              </span>
    </a>
    <!-- End View More - Link -->
</div>
