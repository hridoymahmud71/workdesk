<div class="row mx-gutters-2 mb-6">
    <div class="col-5 col-sm-3">
        <!-- Gallery -->
        <a class="js-fancybox u-media-viewer" href="javascript:;"
           data-src="{{ URL::asset('front-v2.5.0/assets/img/1920x1080/img7.jpg') }}"
           data-fancybox="fancyboxGallery6"
           data-caption="Front in frames - image #01"
           data-speed="700"
           data-is-infinite="true">
            <img class="img-fluid rounded" src="{{ URL::asset('front-v2.5.0/assets/img/380x360/img8.jpg') }}" alt="Image Description">

            <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                      <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                  </span>
        </a>
        <!-- End Gallery -->
    </div>

    <div class="col-5 col-sm-3 d-none d-sm-inline-block">
        <!-- Gallery -->
        <a class="js-fancybox u-media-viewer" href="javascript:;"
           data-src="{{ URL::asset('front-v2.5.0/assets/img/1920x1080/img21.jpg') }}"
           data-fancybox="fancyboxGallery6"
           data-caption="Front in frames - image #02"
           data-speed="700"
           data-is-infinite="true">
            <img class="img-fluid rounded" src="{{ URL::asset('front-v2.5.0/assets/img/380x360/img22.jpg') }}" alt="Image Description">

            <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                      <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                  </span>
        </a>
        <!-- End Gallery -->
    </div>

    <div class="col-5 col-sm-3 d-none d-sm-inline-block">
        <!-- Gallery -->
        <a class="js-fancybox u-media-viewer" href="javascript:;"
           data-src="{{ URL::asset('front-v2.5.0/assets/img/1920x1080/img34.jpg') }}"
           data-fancybox="fancyboxGallery6"
           data-caption="Front in frames - image #03"
           data-speed="700"
           data-is-infinite="true">
            <img class="img-fluid rounded" src="{{ URL::asset('front-v2.5.0/assets/img/380x360/img27.jpg') }}" alt="Image Description">

            <span class="u-media-viewer__container">
                    <span class="u-media-viewer__icon">
                      <span class="fas fa-plus u-media-viewer__icon-inner"></span>
                    </span>
                  </span>
        </a>
        <!-- End Gallery -->
    </div>

    <div class="col-5 col-sm-3">
        <!-- Gallery -->
        <a class="js-fancybox u-media-viewer" href="javascript:;"
           data-src="{{ URL::asset('front-v2.5.0/assets/img/1920x1080/img35.jpg') }}"
           data-fancybox="fancyboxGallery6"
           data-caption="Front in frames - image #04"
           data-speed="700"
           data-is-infinite="true">
            <img class="img-fluid rounded" src="{{ URL::asset('front-v2.5.0/assets/img/380x360/img31.jpg') }}" alt="Image Description">

            <span class="u-media-viewer__container">
                    <span class="d-none d-sm-inline-block u-media-viewer__icon u-media-viewer__icon--active">
                      <span class="u-media-viewer__icon-inner font-weight-medium">+3</span>
                    </span>
                    <span class="d-sm-none u-media-viewer__icon u-media-viewer__icon--active">
                      <span class="u-media-viewer__icon-inner font-weight-medium">+5</span>
                    </span>
                  </span>
        </a>
        <!-- End Gallery -->

        <img class="js-fancybox d-none" alt="Image Description"
             data-fancybox="fancyboxGallery6"
             data-src="{{ URL::asset('front-v2.5.0/assets/img/1920x1080/img2.jpg') }}"
             data-caption="Front in frames - image #05"
             data-speed="700"
             data-is-infinite="true">
        <img class="js-fancybox d-none" alt="Image Description"
             data-caption="Front in frames - image #06"
             data-src="{{ URL::asset('front-v2.5.0/assets/img/1920x1080/img29.jpg') }}"
             data-fancybox="fancyboxGallery6"
             data-speed="700"
             data-is-infinite="true">
        <img class="js-fancybox d-none" alt="Image Description"
             data-fancybox="fancyboxGallery6"
             data-src="{{ URL::asset('front-v2.5.0/assets/img/1920x1080/img24.jpg') }}"
             data-caption="Front in frames - image #07"
             data-speed="700"
             data-is-infinite="true">
    </div>
</div>
