<form action="{{URL::to('/project_search')}}">
    <div class="border-bottom pb-4 mb-4">
        <h4 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Cost</h4>
        <input type="hidden" name="name" value="{{Input::has('name')?Input::get('name'):''}}">
        <input type="hidden" name="sort_by" value="{{Input::has('sort_by')?Input::get('sort_by'):''}}">
        <!-- Range Slider -->
        <input class="js-range-slider" type="text"
               data-extra-classes="u-range-slider u-range-slider-indicator u-range-slider-grid"
               data-type="double"
               data-grid="true"
               data-hide-from-to="false"
               data-prefix="$"
               data-min="0"
               data-max="10000"
               data-from="2500"
               data-to="7500"
               data-result-min="#rangeSliderExample3MinResult"
               data-result-max="#rangeSliderExample3MaxResult">
        <div class="d-flex justify-content-between mt-4">
            <input type="text" name="max_cost" value="{{Input::has('max_cost')?Input::get('max_cost'):''}}"
                   class="form-control form-control-sm max-width-10" id="rangeSliderExample3MinResult">
            <input type="text" name="min_cost" value="{{Input::has('min_cost')?Input::get('min_cost'):''}}"
                   class="form-control form-control-sm max-width-10 mt-0" id="rangeSliderExample3MaxResult">
        </div>
        <!-- End Range Slider -->
    </div>

    <div class="border-bottom pb-4 mb-4">
        <h4 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Type</h4>

    @php
        $types =
        [
            0=>['key'=>"tp1",'name'=>"Type 1"],
            1=>['key'=>"tp2",'name'=>"Type 2"],
            2=>['key'=>"tp3",'name'=>"Type 3"],
            3=>['key'=>"tp4",'name'=>"Type 4"],
        ];
    @endphp

    <!-- Full Time Project Type Checkbox -->
        @if(!empty($types))
            @foreach($types as $k => $type)
                <div class="form-group d-flex align-items-center justify-content-between font-size-1 text-lh-md text-secondary mb-2">
                    <div class="custom-control custom-radio">
                        @php $input_type = Input::has('type')?Input::get('type'):''; @endphp
                        <input type="radio" name="type" value="{{$type['key']}}" class="custom-control-input" id="{{$type['key']}}" {{ $input_type == $type['key'] ? " checked " : "" }}>
                        <label class="custom-control-label" for="{{$type['key']}}">{{$type['name']}}</label>
                    </div>
                    <small>{{rand(0,1000)}}</small>
                </div>
        @endforeach
    @endif
    <!-- End Full Time Project Type Checkbox -->
    </div>

    <div class="border-bottom pb-4 mb-4">
        <h4 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Category</h4>


        <!-- Category Development Checkbox -->
    @php
        $input_categories = Input::has('categories')?Input::get('categories'):[];

        $categories =
        [
            0=>['key'=>"cat1",'name'=>"Category 1"],
            1=>['key'=>"cat2",'name'=>"Category 2"],
            2=>['key'=>"cat3",'name'=>"Category 3"],
            3=>['key'=>"cat4",'name'=>"Category 4"],
            4=>['key'=>"cat5",'name'=>"Category 5"],
            5=>['key'=>"cat6",'name'=>"Category 6"],
        ];

        //$categories = [];

        //show 1/3 hide 2/3 or you can set a fix point like show 5 items code: !empty($categories) ? 4 : 0;
        $categories_slice_point = !empty($categories) ? intval(count($categories)/3) : 0;
        $categories_slice[] = !empty($categories) ? array_slice($categories,0,$categories_slice_point,true) : [];
        $categories_slice[] = !empty($categories) ? array_slice($categories,$categories_slice_point,count($categories),true) : [];
    @endphp

    <!-- <pre>
                                {{--{{print_r($categories_slice)}}--}}
        </pre> -->

        @if(isset($categories_slice[0]) && !empty($categories_slice[0]))
            @foreach($categories_slice[0] as $k => $cateogry)
                <div class="form-group d-flex align-items-center justify-content-between font-size-1 text-lh-md text-secondary mb-2">
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" name="categories[]" value="{{$cateogry['key']}}"
                               class="custom-control-input" id="{{$cateogry['key']}}" {{ in_array($cateogry['key'],$input_categories)  ? " checked " : "" }}>
                        <label class="custom-control-label" for="{{$cateogry['key']}}">{{$cateogry['name']}}</label>
                    </div>
                    <small>{{rand(0,250)}}</small>
                </div>
            @endforeach
        @endif

        <div class="collapse" id="collapseCategory">
            @if(isset($categories_slice[1]) && !empty($categories_slice[1]))
                @foreach($categories_slice[1] as $k => $cateogry)

                    <div class="form-group d-flex align-items-center justify-content-between font-size-1 text-lh-md text-secondary mb-2">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" name="categories[]" value="{{$cateogry['key']}}"
                                   class="custom-control-input" id="{{$cateogry['key']}}" {{ in_array($cateogry['key'],$input_categories)  ? " checked " : "" }}>
                            <label class="custom-control-label" for="{{$cateogry['key']}}">{{$cateogry['name']}}</label>
                        </div>
                        <small>{{rand(0,250)}}</small>
                    </div>
            @endforeach
        @endif
        <!-- End Category Development Checkbox -->
        </div>

    @if(isset($categories_slice[1]) && !empty($categories_slice[1]))
        <!-- Link -->
            <a class="link link-collapse small font-size-1" data-toggle="collapse" href="#collapseCategory" role="button" aria-expanded="false" aria-controls="collapseCategory">
                <span class="link-collapse__default">{{__('More')}}</span>
                <span class="link-collapse__active">{{__('Less')}}</span>
                <span class="link__icon ml-1">
                                <span class="link__icon-inner">+</span>
                            </span>
            </a>
    @endif



    <!-- End Category Marketing Checkbox -->
    </div>

    <button type="submit" class="btn btn-sm btn-block btn-soft-primary transition-3d-hover">{{__('Submit')}}</button>
    <button type="button" class="btn btn-sm btn-block btn-soft-secondary transition-3d-hover" onclick="location.reload()">{{__('Reset')}}</button>
</form>
