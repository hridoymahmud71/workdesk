<!-- Projects -->
<div class="card mb-5">
    <div class="card-body p-4">
        <!-- Details -->
        <div class="media d-block d-sm-flex">
            <div class="u-avatar mb-3 mb-sm-0 mr-4">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img14.png') }}" alt="Image Description">
            </div>

            <div class="media-body">
                <!-- Header -->
                <div class="media mb-2">
                    <div class="media-body">
                        <h2 class="h5 mb-1">
                            <a href="company-profile.html">Senior Java Developer</a>
                        </h2>
                        <ul class="list-inline font-size-1 text-muted">
                            <li class="list-inline-item">
                                <a class="link-muted" href="job-description.html">
                                    <span class="fas fa-building mr-1"></span>
                                    Slack LLC
                                </a>
                            </li>
                            <li class="list-inline-item text-muted">&#8226;</li>
                            <li class="list-inline-item">Office</li>
                        </ul>
                    </div>

                    <div class="d-flex ml-auto">
                        <!-- Rating -->
                        <div class="position-relative d-inline-block mr-2">
                            <a id="rating1DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                               aria-controls="rating1Dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                               data-unfold-event="hover"
                               data-unfold-target="#rating1Dropdown"
                               data-unfold-type="css-animation"
                               data-unfold-duration="300"
                               data-unfold-delay="300"
                               data-unfold-hide-on-scroll="true"
                               data-unfold-animation-in="slideInUp"
                               data-unfold-animation-out="fadeOut">
                                4.5
                            </a>

                            <div id="rating1Dropdown" class="dropdown-menu dropdown-menu-right dropdown-unfold p-3" aria-labelledby="rating1DropdownInvoker" style="width: 190px;">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="text-warning mr-2">4.5</span>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item text-warning">
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star-half-alt"></span>
                                        </li>
                                    </ul>
                                </div>

                                <p class="text-dark mb-0">Overal Rating</p>
                                <p class="mb-0">Based on 132 reviews</p>
                            </div>
                        </div>
                        <!-- End Rating -->

                        <!-- Bookmark -->
                        <div class="bookmark-checkbox d-inline-block" data-toggle="tooltip" data-placement="top" title="Save Project">
                            <input type="checkbox" class="bookmark-checkbox-input" id="bookmark1">
                            <label class="bookmark-checkbox-label" for="bookmark1"></label>
                        </div>
                        <!-- End Bookmark -->
                    </div>
                </div>
                <!-- End Header -->

                <div class="mb-5">
                    <p>The role is responsible for designing, coding and modifying websites, from layout to function and according to a client's specifications. Strive to create visually appealing sites that feature user-friendly design and clear navigation.</p>
                </div>

                <div class="d-md-flex align-items-md-center">
                    <!-- Location -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h3 class="small text-secondary mb-0">Location</h3>
                        <small class="fas fa-map-marker-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">Liverpool, UK</span>
                    </div>
                    <!-- End Location -->

                    <!-- Posted -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Posted</h4>
                        <small class="fas fa-calendar-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">1 day ago</span>
                    </div>
                    <!-- End Posted -->

                    <!-- Posted -->
                    <div class="mb-3 mb-md-0">
                        <h4 class="small text-secondary mb-0">Salary</h4>
                        <small class="fas fa-dollar-sign text-secondary align-middle mr-1"></small>
                        <span class="align-middle">35k - 45k</span>
                    </div>
                    <!-- End Posted -->

                    <div class="ml-md-auto">
                        <span class="btn btn-xs btn-soft-danger btn-pill">Fulltime</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Details -->
    </div>
</div>
<!-- End Projects -->

<!-- Projects -->
<div class="card mb-5">
    <div class="card-body p-4">
        <!-- Details -->
        <div class="media d-block d-sm-flex">
                <span class="btn btn-icon btn-soft-primary rounded-circle shadow-soft mb-3 mb-sm-0 mr-4">
                  <span class="btn-icon__inner">DB</span>
                </span>

            <div class="media-body">
                <!-- Header -->
                <div class="media mb-2">
                    <div class="media-body">
                        <h3 class="h5 mb-1">
                            <a href="company-profile.html">Channel Sales Director</a>
                        </h3>
                        <ul class="list-inline font-size-1 text-muted">
                            <li class="list-inline-item">
                                <a class="link-muted" href="job-description.html">
                                    <span class="fas fa-building mr-1"></span>
                                    Dropbox
                                </a>
                            </li>
                            <li class="list-inline-item text-muted">&#8226;</li>
                            <li class="list-inline-item">Remote</li>
                        </ul>
                    </div>

                    <div class="d-flex ml-auto">
                        <!-- Rating -->
                        <div class="position-relative d-inline-block mr-2">
                            <a id="rating2DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                               aria-controls="rating2Dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                               data-unfold-event="hover"
                               data-unfold-target="#rating2Dropdown"
                               data-unfold-type="css-animation"
                               data-unfold-duration="300"
                               data-unfold-delay="300"
                               data-unfold-hide-on-scroll="true"
                               data-unfold-animation-in="slideInUp"
                               data-unfold-animation-out="fadeOut">
                                4.7
                            </a>

                            <div id="rating2Dropdown" class="dropdown-menu dropdown-menu-right dropdown-unfold p-3" aria-labelledby="rating2DropdownInvoker" style="width: 190px;">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="text-warning mr-2">4.7</span>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item text-warning">
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star-half-alt"></span>
                                        </li>
                                    </ul>
                                </div>

                                <p class="text-dark mb-0">Overal Rating</p>
                                <p class="mb-0">Based on 120 reviews</p>
                            </div>
                        </div>
                        <!-- End Rating -->

                        <!-- Bookmark -->
                        <div class="bookmark-checkbox d-inline-block" data-toggle="tooltip" data-placement="top" title="Save Project">
                            <input type="checkbox" class="bookmark-checkbox-input" id="bookmark2">
                            <label class="bookmark-checkbox-label" for="bookmark2"></label>
                        </div>
                        <!-- End Bookmark -->
                    </div>
                </div>
                <!-- End Header -->

                <div class="mb-5">
                    <p>Participates in all aspects of full life cycle application development including requirements gathering, technical design, iterative development, testing, implementation and on-going support.</p>
                </div>

                <div class="d-md-flex align-items-md-center">
                    <!-- Location -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Location</h4>
                        <small class="fas fa-map-marker-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">Leeds, UK</span>
                    </div>
                    <!-- End Location -->

                    <!-- Posted -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Posted</h4>
                        <small class="fas fa-calendar-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">2 days ago</span>
                    </div>
                    <!-- End Posted -->

                    <!-- Posted -->
                    <div class="mb-3 mb-md-0">
                        <h4 class="small text-secondary mb-0">Salary</h4>
                        <small class="fas fa-dollar-sign text-secondary align-middle mr-1"></small>
                        <span class="align-middle">55k - 65k</span>
                    </div>
                    <!-- End Posted -->

                    <div class="ml-md-auto">
                        <span class="btn btn-xs btn-soft-danger btn-pill">Contract</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Details -->
    </div>
</div>
<!-- End Projects -->

<!-- Projects -->
<div class="card mb-5">
    <div class="card-body p-4">
        <!-- Details -->
        <div class="media d-block d-sm-flex">
            <div class="u-avatar mb-3 mb-sm-0 mr-4">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img9.jpg') }}" alt="Image Description">
            </div>

            <div class="media-body">
                <!-- Header -->
                <div class="media mb-2">
                    <div class="media-body">
                        <h3 class="h5 mb-1">
                            <a href="company-profile.html">Junior UX Designer</a>
                        </h3>
                        <ul class="list-inline font-size-1 text-muted">
                            <li class="list-inline-item">
                                <a class="link-muted" href="job-description.html">
                                    <span class="fas fa-building mr-1"></span>
                                    Dribbble
                                </a>
                            </li>
                            <li class="list-inline-item text-muted">&#8226;</li>
                            <li class="list-inline-item">Office</li>
                        </ul>
                    </div>

                    <div class="d-flex ml-auto">
                        <!-- Rating -->
                        <div class="position-relative d-inline-block mr-2">
                            <a id="rating3DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                               aria-controls="rating3Dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                               data-unfold-event="hover"
                               data-unfold-target="#rating3Dropdown"
                               data-unfold-type="css-animation"
                               data-unfold-duration="300"
                               data-unfold-delay="300"
                               data-unfold-hide-on-scroll="true"
                               data-unfold-animation-in="slideInUp"
                               data-unfold-animation-out="fadeOut">
                                4.5
                            </a>

                            <div id="rating3Dropdown" class="dropdown-menu dropdown-menu-right dropdown-unfold p-3" aria-labelledby="rating3DropdownInvoker" style="width: 190px;">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="text-warning mr-2">4.5</span>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item text-warning">
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star-half-alt"></span>
                                        </li>
                                    </ul>
                                </div>

                                <p class="text-dark mb-0">Overal Rating</p>
                                <p class="mb-0">Based on 85 reviews</p>
                            </div>
                        </div>
                        <!-- End Rating -->

                        <!-- Bookmark -->
                        <div class="bookmark-checkbox d-inline-block" data-toggle="tooltip" data-placement="top" title="Save Project">
                            <input type="checkbox" class="bookmark-checkbox-input" id="bookmark3">
                            <label class="bookmark-checkbox-label" for="bookmark3"></label>
                        </div>
                        <!-- End Bookmark -->
                    </div>
                </div>
                <!-- End Header -->

                <div class="mb-5">
                    <p>We are looking for an outstanding Web Developer to be responsible for coding, and completing layouts of client and internal websites. You will build websites from concept all the way to completion from the bottom up.</p>
                </div>

                <div class="d-md-flex align-items-md-center">
                    <!-- Location -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Location</h4>
                        <small class="fas fa-map-marker-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">Manchester, UK</span>
                    </div>
                    <!-- End Location -->

                    <!-- Posted -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Posted</h4>
                        <small class="fas fa-calendar-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">3 days ago</span>
                    </div>
                    <!-- End Posted -->

                    <!-- Posted -->
                    <div class="mb-3 mb-md-0">
                        <h4 class="small text-secondary mb-0">Salary</h4>
                        <small class="fas fa-dollar-sign text-secondary align-middle mr-1"></small>
                        <span class="align-middle">15k - 30k</span>
                    </div>
                    <!-- End Posted -->

                    <div class="ml-md-auto">
                        <span class="btn btn-xs btn-soft-danger btn-pill">Contract</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Details -->
    </div>
</div>
<!-- End Projects -->

<!-- Projects -->
<div class="card mb-5">
    <div class="card-body p-4">
        <!-- Details -->
        <div class="media d-block d-sm-flex">
            <div class="u-avatar mb-3 mb-sm-0 mr-4">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img11.jpg') }}" alt="Image Description">
            </div>

            <div class="media-body">
                <!-- Header -->
                <div class="media mb-2">
                    <div class="media-body">
                        <h3 class="h5 mb-1">
                            <a href="company-profile.html">Cloud Software Engineer</a>
                        </h3>
                        <ul class="list-inline font-size-1 text-muted">
                            <li class="list-inline-item">
                                <a class="link-muted" href="job-description.html">
                                    <span class="fas fa-building mr-1"></span>
                                    Adobe Inc
                                </a>
                            </li>
                            <li class="list-inline-item text-muted">&#8226;</li>
                            <li class="list-inline-item">Office</li>
                        </ul>
                    </div>

                    <div class="d-flex ml-auto">
                        <!-- Rating -->
                        <div class="position-relative d-inline-block mr-2">
                            <a id="rating4DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                               aria-controls="rating4Dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                               data-unfold-event="hover"
                               data-unfold-target="#rating4Dropdown"
                               data-unfold-type="css-animation"
                               data-unfold-duration="300"
                               data-unfold-delay="300"
                               data-unfold-hide-on-scroll="true"
                               data-unfold-animation-in="slideInUp"
                               data-unfold-animation-out="fadeOut">
                                4.4
                            </a>

                            <div id="rating4Dropdown" class="dropdown-menu dropdown-menu-right dropdown-unfold p-3" aria-labelledby="rating4DropdownInvoker" style="width: 190px;">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="text-warning mr-2">4.4</span>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item text-warning">
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="far fa-star"></span>
                                            <span class="far fa-star"></span>
                                        </li>
                                    </ul>
                                </div>

                                <p class="text-dark mb-0">Overal Rating</p>
                                <p class="mb-0">Based on 5 reviews</p>
                            </div>
                        </div>
                        <!-- End Rating -->

                        <!-- Bookmark -->
                        <div class="bookmark-checkbox d-inline-block" data-toggle="tooltip" data-placement="top" title="Save Project">
                            <input type="checkbox" class="bookmark-checkbox-input" id="bookmark4">
                            <label class="bookmark-checkbox-label" for="bookmark4"></label>
                        </div>
                        <!-- End Bookmark -->
                    </div>
                </div>
                <!-- End Header -->

                <div class="mb-5">
                    <p>Partner with Marketing and Design Leads to manage multiple bodies of work and align against business goals. Clearly communicate with all stakeholders while recognizing potential risks and opportunities to obtain the best results.</p>
                </div>

                <div class="d-md-flex align-items-md-center">
                    <!-- Location -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Location</h4>
                        <small class="fas fa-map-marker-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">London, UK</span>
                    </div>
                    <!-- End Location -->

                    <!-- Posted -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Posted</h4>
                        <small class="fas fa-calendar-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">3 days ago</span>
                    </div>
                    <!-- End Posted -->

                    <!-- Posted -->
                    <div class="mb-3 mb-md-0">
                        <h4 class="small text-secondary mb-0">Salary</h4>
                        <small class="fas fa-dollar-sign text-secondary align-middle mr-1"></small>
                        <span class="align-middle">20k - 28k</span>
                    </div>
                    <!-- End Posted -->

                    <div class="ml-md-auto">
                        <span class="btn btn-xs btn-soft-danger btn-pill">Part-time</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Details -->
    </div>
</div>
<!-- End Projects -->

<!-- Projects -->
<div class="card mb-5">
    <div class="card-body p-4">
        <!-- Details -->
        <div class="media d-block d-sm-flex">
                <span class="btn btn-icon btn-soft-danger rounded-circle shadow-soft mb-3 mb-sm-0 mr-4">
                  <span class="btn-icon__inner">AB</span>
                </span>

            <div class="media-body">
                <!-- Header -->
                <div class="media mb-2">
                    <div class="media-body">
                        <h3 class="h5 mb-1">
                            <a href="company-profile.html">Database Analyst</a>
                        </h3>
                        <ul class="list-inline font-size-1 text-muted">
                            <li class="list-inline-item">
                                <a class="link-muted" href="job-description.html">
                                    <span class="fas fa-building mr-1"></span>
                                    Airbnb Inc
                                </a>
                            </li>
                            <li class="list-inline-item text-muted">&#8226;</li>
                            <li class="list-inline-item">Remote</li>
                        </ul>
                    </div>

                    <div class="d-flex ml-auto">
                        <!-- Rating -->
                        <div class="position-relative d-inline-block mr-2">
                            <a id="rating5DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                               aria-controls="rating5Dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                               data-unfold-event="hover"
                               data-unfold-target="#rating5Dropdown"
                               data-unfold-type="css-animation"
                               data-unfold-duration="300"
                               data-unfold-delay="300"
                               data-unfold-hide-on-scroll="true"
                               data-unfold-animation-in="slideInUp"
                               data-unfold-animation-out="fadeOut">
                                5.0
                            </a>

                            <div id="rating5Dropdown" class="dropdown-menu dropdown-menu-right dropdown-unfold p-3" aria-labelledby="rating5DropdownInvoker" style="width: 190px;">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="text-warning mr-2">5.0</span>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item text-warning">
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                        </li>
                                    </ul>
                                </div>

                                <p class="text-dark mb-0">Overal Rating</p>
                                <p class="mb-0">Based on 1809 reviews</p>
                            </div>
                        </div>
                        <!-- End Rating -->

                        <!-- Bookmark -->
                        <div class="bookmark-checkbox d-inline-block" data-toggle="tooltip" data-placement="top" title="Save Project">
                            <input type="checkbox" class="bookmark-checkbox-input" id="bookmark5">
                            <label class="bookmark-checkbox-label" for="bookmark5"></label>
                        </div>
                        <!-- End Bookmark -->
                    </div>
                </div>
                <!-- End Header -->

                <div class="mb-5">
                    <p>We looking for a talented and enthusiastic Junior Front End Web Developer to join our busy team. The position will be an integral member of the Creative Services team, working collaboratively with Marketing and Engineering departments.</p>
                </div>

                <div class="d-md-flex align-items-md-center">
                    <!-- Location -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Location</h4>
                        <small class="fas fa-map-marker-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">Edimbourg, UK</span>
                    </div>
                    <!-- End Location -->

                    <!-- Posted -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Posted</h4>
                        <small class="fas fa-calendar-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">4 days ago</span>
                    </div>
                    <!-- End Posted -->

                    <!-- Posted -->
                    <div class="mb-3 mb-md-0">
                        <h4 class="small text-secondary mb-0">Salary</h4>
                        <small class="fas fa-dollar-sign text-secondary align-middle mr-1"></small>
                        <span class="align-middle">40k - 49k</span>
                    </div>
                    <!-- End Posted -->

                    <div class="ml-md-auto">
                        <span class="btn btn-xs btn-soft-danger btn-pill">Contract</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Details -->
    </div>
</div>
<!-- End Projects -->

<!-- Projects -->
<div class="card mb-5">
    <div class="card-body p-4">
        <!-- Details -->
        <div class="media d-block d-sm-flex">
            <div class="u-avatar mb-3 mb-sm-0 mr-4">
                <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img15.png') }}" alt="Image Description">
            </div>

            <div class="media-body">
                <!-- Header -->
                <div class="media mb-2">
                    <div class="media-body">
                        <h3 class="h5 mb-1">
                            <a href="company-profile.html">Product Director</a>
                        </h3>
                        <ul class="list-inline font-size-1 text-muted">
                            <li class="list-inline-item">
                                <a class="link-muted" href="job-description.html">
                                    <span class="fas fa-building mr-1"></span>
                                    Spotify Inc
                                </a>
                            </li>
                            <li class="list-inline-item text-muted">&#8226;</li>
                            <li class="list-inline-item">Remote</li>
                        </ul>
                    </div>

                    <div class="d-flex ml-auto">
                        <!-- Rating -->
                        <div class="position-relative d-inline-block mr-2">
                            <a id="rating6DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                               aria-controls="rating6Dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                               data-unfold-event="hover"
                               data-unfold-target="#rating6Dropdown"
                               data-unfold-type="css-animation"
                               data-unfold-duration="300"
                               data-unfold-delay="300"
                               data-unfold-hide-on-scroll="true"
                               data-unfold-animation-in="slideInUp"
                               data-unfold-animation-out="fadeOut">
                                4.4
                            </a>

                            <div id="rating6Dropdown" class="dropdown-menu dropdown-menu-right dropdown-unfold p-3" aria-labelledby="rating6DropdownInvoker" style="width: 190px;">
                                <div class="d-flex align-items-center mb-2">
                                    <span class="text-warning mr-2">4.4</span>
                                    <ul class="list-inline mb-0">
                                        <li class="list-inline-item text-warning">
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="fas fa-star"></span>
                                            <span class="far fa-star"></span>
                                            <span class="far fa-star"></span>
                                        </li>
                                    </ul>
                                </div>

                                <p class="text-dark mb-0">Overal Rating</p>
                                <p class="mb-0">Based on 427 reviews</p>
                            </div>
                        </div>
                        <!-- End Rating -->

                        <!-- Bookmark -->
                        <div class="bookmark-checkbox d-inline-block" data-toggle="tooltip" data-placement="top" title="Save Project">
                            <input type="checkbox" class="bookmark-checkbox-input" id="bookmark6">
                            <label class="bookmark-checkbox-label" for="bookmark6"></label>
                        </div>
                        <!-- End Bookmark -->
                    </div>
                </div>
                <!-- End Header -->

                <div class="mb-5">
                    <p>We focus exclusively on helping government agencies and companies implement their most critically strategic initiatives. The role of Web Developer contributes through the design, development, and maintenance of contract applications.</p>
                </div>

                <div class="d-md-flex align-items-md-center">
                    <!-- Location -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Location</h4>
                        <small class="fas fa-map-marker-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">London, UK</span>
                    </div>
                    <!-- End Location -->

                    <!-- Posted -->
                    <div class="u-ver-divider u-ver-divider--none-md pr-4 mb-3 mb-md-0 mr-4">
                        <h4 class="small text-secondary mb-0">Posted</h4>
                        <small class="fas fa-calendar-alt text-secondary align-middle mr-1"></small>
                        <span class="align-middle">5 days ago</span>
                    </div>
                    <!-- End Posted -->

                    <!-- Posted -->
                    <div class="mb-3 mb-md-0">
                        <h4 class="small text-secondary mb-0">Salary</h4>
                        <small class="fas fa-dollar-sign text-secondary align-middle mr-1"></small>
                        <span class="align-middle">40k - 49k</span>
                    </div>
                    <!-- End Posted -->

                    <div class="ml-md-auto">
                        <span class="btn btn-xs btn-soft-danger btn-pill">Part-time</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Details -->
    </div>
</div>
<!-- End Projects -->

<div class="py-3"></div>

<!-- Pagination -->
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center mb-0">
        <li class="page-item ml-0">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item disabled"><a class="page-link" href="#">...</a></li>
        <li class="page-item"><a class="page-link" href="#">12</a></li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
    </ul>
</nav>
<!-- End Pagination -->
