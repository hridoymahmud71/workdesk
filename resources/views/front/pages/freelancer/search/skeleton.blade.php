@extends('front.master')
@section('content')
    <!-- Hero Section -->
    <div class="bg-light">
        <div class="container space-1">
            <!-- Search Freelancers Form -->
            @include('front.forms.freelancer_search_horizontal')
            <!-- End Search Freelancers Form -->
        </div>
    </div>
    <!-- End Hero Section -->

    <!-- Freelancers Section -->
    <div class="container space-2">
        <div class="row">
            <div class="col-lg-3"></div>
            <div class="col-lg-9">
                <!-- Filter -->
                <div class="row justify-content-between align-items-center mb-4">
                    <!-- Title -->
                    <div class="col-sm-4 col-md-6 mb-3 mb-sm-0">
                        @php $freelancer_count = rand(0,50);
                        $freelancer_count_plurality = plurality($freelancer_count)
                        @endphp

                        <h1 class="h5 mb-0">{{__(":freelancer_count related freelancer{$freelancer_count_plurality['s']} {$freelancer_count_plurality['is_or_are']}  Found ",[ 'freelancer_count' => $freelancer_count])}}</h1>
                    </div>
                    <!-- End Title -->

                    <div class="col-sm-8 col-md-6 text-sm-right">
                        <form class="js-validate" action="{{URL::to('/freelancer_search')}}" >
                            <input type="hidden" name="name" value="{{Input::has('name')?Input::get('name'):''}}">
                            <input type="hidden" name="type" value="{{Input::has('type')?Input::get('type'):''}}">
                            @php $input_categories = Input::has('categories')?Input::get('categories'):[];@endphp
                            @if(!empty($input_categories))
                                @foreach($input_categories as $input_category)
                                <input type="hidden" name="categories[]" value="{{$input_category}}">
                                @endforeach
                            @endif
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item">
                                    <!-- Select -->
                                    <select name="sort_by" class="js-select selectpicker dropdown-select freelancer_sort"
                                            data-width="fit"
                                            data-style="btn-soft-primary btn-xs">
                                        <option value="" selected>{{__('Sort By')}}</option>
                                        <option value="date_desc">{{__('Newer first')}}</option>
                                        <option value="date_asc">{{__('Older First')}}</option>
                                        <option value="cost_desc">{{__('Price: High to Low')}}</option>
                                        <option value="cost_asc">{{__('Price: Low to High')}}</option>
                                    </select>
                                    <!-- End Select -->
                                </li>
                            </ul>
                        </form>
                    </div>
                </div>
                <!-- End Filter -->
            </div>
        </div>

        <div class="row">
            <!-- Filters -->
            <div class="col-lg-3">
                @include('front.pages.freelancer.search.segments.filter')
            </div>
            <!-- End Filters -->
            <!-- Freelancers -->
            <div class="col-lg-9">
            @include('front.pages.freelancer.search.segments.list')
            </div>
            <!-- End Freelancers -->
        </div>
    </div>
    <!-- End Freelancers Section -->
@endsection

@section('in_blade_js_codes')
    <script>
        $(document).on('ready', function () {

            $(".freelancer_sort").on("change", function (e) {

                $(this).closest('form').submit();

            })
        });
    </script>
@endsection


