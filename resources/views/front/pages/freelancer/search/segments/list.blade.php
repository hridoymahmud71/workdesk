<div class="card-deck d-block d-md-flex">
    <!-- Item -->
    <div class="card mb-5">
        <div class="card-body p-4">
            <!-- Header -->
            <div class="d-flex align-items-center mb-5">
                <!-- Rating -->
                <div class="position-relative">
                    <a id="rating1DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                       aria-controls="rating1Dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-unfold-event="hover"
                       data-unfold-target="#rating1Dropdown"
                       data-unfold-type="css-animation"
                       data-unfold-duration="300"
                       data-unfold-delay="300"
                       data-unfold-hide-on-scroll="true"
                       data-unfold-animation-in="slideInUp"
                       data-unfold-animation-out="fadeOut">
                        4.5
                    </a>

                    <div id="rating1Dropdown" class="dropdown-menu dropdown-unfold p-3" aria-labelledby="rating1DropdownInvoker" style="width: 190px;">
                        <div class="d-flex align-items-center mb-2">
                            <span class="text-warning mr-2">4.5</span>
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item text-warning">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star-half-alt"></span>
                                </li>
                            </ul>
                        </div>

                        <p class="text-dark mb-0">Overal Rating</p>
                        <p class="mb-0">Based on 132 reviews</p>
                    </div>
                </div>
                <!-- End Rating -->

                <!-- Bookmark -->
                <div class="ml-auto">
                    <div class="bookmark-checkbox" data-toggle="tooltip" data-placement="top" title="Save">
                        <input type="checkbox" class="bookmark-checkbox-input" id="bookmark2">
                        <label class="bookmark-checkbox-label" for="bookmark2"></label>
                    </div>
                </div>
                <!-- End Bookmark -->
            </div>
            <!-- End Header -->

            <div class="text-center">
                <!-- Avatar -->
                <div class="u-lg-avatar position-relative mx-auto mb-3">
                    <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/100x100/img9.jpg') }}" alt="Image Description">
                    <span class="badge badge-xs badge-outline-success badge-pos badge-pos--bottom-left rounded-circle"></span>
                </div>
                <!-- End Avatar -->

                <!-- Title -->
                <div class="mb-4">
                    <h1 class="h5 mb-0">
                        <a href="employee-profile.html">Jorge Fields</a>
                    </h1>
                    <small class="text-secondary">Apps and Web developer</small>
                </div>
                <!-- End Title -->

                <p class="mb-0">I can start working right now and I can WORK FULL TIME on your project during the entire development of your project.</p>
            </div>
        </div>

        <div class="card-footer text-center py-4">
            <!-- Location & Rate -->
            <div class="row align-items-center">
                <div class="col-6 u-ver-divider">
                    <h2 class="small text-secondary mb-0">Location</h2>
                    <img class="img-fluid max-width-3 rounded-circle align-middle mr-1" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/1x1/gb.svg') }}" alt="United Kingdom Flag">
                    <span class="align-middle">London</span>
                </div>
                <div class="col-6">
                    <h3 class="small text-secondary mb-0">Working rate</h3>
                    <span class="align-middle">$35/hr</span>
                </div>
            </div>
            <!-- End Location & Rate -->
        </div>
    </div>
    <!-- End Item -->

    <!-- Item -->
    <div class="card mb-5">
        <div class="card-body p-4">
            <!-- Header -->
            <div class="d-flex align-items-center mb-5">
                <!-- Rating -->
                <div class="position-relative">
                    <a id="rating2DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                       aria-controls="rating2Dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-unfold-event="hover"
                       data-unfold-target="#rating2Dropdown"
                       data-unfold-type="css-animation"
                       data-unfold-duration="300"
                       data-unfold-delay="300"
                       data-unfold-hide-on-scroll="true"
                       data-unfold-animation-in="slideInUp"
                       data-unfold-animation-out="fadeOut">
                        4.7
                    </a>

                    <div id="rating2Dropdown" class="dropdown-menu dropdown-unfold p-3" aria-labelledby="rating2DropdownInvoker" style="width: 190px;">
                        <div class="d-flex align-items-center mb-2">
                            <span class="text-warning mr-2">4.7</span>
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item text-warning">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star-half-alt"></span>
                                </li>
                            </ul>
                        </div>

                        <p class="text-dark mb-0">Overal Rating</p>
                        <p class="mb-0">Based on 120 reviews</p>
                    </div>
                </div>
                <!-- End Rating -->

                <!-- Bookmark -->
                <div class="ml-auto">
                    <div class="bookmark-checkbox" data-toggle="tooltip" data-placement="top" title="Save">
                        <input type="checkbox" class="bookmark-checkbox-input" id="bookmark3">
                        <label class="bookmark-checkbox-label" for="bookmark3"></label>
                    </div>
                </div>
                <!-- End Bookmark -->
            </div>
            <!-- End Header -->

            <div class="text-center">
                <!-- Avatar -->
                <span class="btn btn-lg btn-icon btn-soft-primary rounded-circle mb-3">
                    <span class="btn-icon__inner">BS</span>
                    <span class="badge badge-xs badge-outline-success badge-pos badge-pos--bottom-left rounded-circle"></span>
                  </span>
                <!-- End Avatar -->

                <!-- Title -->
                <div class="mb-4">
                    <h3 class="h5 mb-0">
                        <a href="employee-profile.html">Bertha Sandoval</a>
                    </h3>
                    <small class="text-secondary">Senior Android, iOS, Web Developer</small>
                </div>
                <!-- End Title -->

                <p class="mb-0">Hello and welcome! I am an Web Developer with more than 5 years of commercial experience in this area.</p>
            </div>
        </div>

        <div class="card-footer text-center py-4">
            <!-- Location & Rate -->
            <div class="row align-items-center">
                <div class="col-6 u-ver-divider">
                    <h4 class="small text-secondary mb-0">Location</h4>
                    <img class="img-fluid max-width-3 rounded-circle align-middle mr-1" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/1x1/gb.svg') }}" alt="United Kingdom Flag">
                    <span class="align-middle">London</span>
                </div>
                <div class="col-6">
                    <h4 class="small text-secondary mb-0">Working rate</h4>
                    <span class="align-middle">$30/hr</span>
                </div>
            </div>
            <!-- End Location & Rate -->
        </div>
    </div>
    <!-- End Item -->
</div>

<div class="card-deck d-block d-md-flex">
    <!-- Item -->
    <div class="card mb-5">
        <div class="card-body p-4">
            <!-- Header -->
            <div class="d-flex align-items-center mb-5">
                <!-- Rating -->
                <div class="position-relative">
                    <a id="rating3DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                       aria-controls="rating3Dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-unfold-event="hover"
                       data-unfold-target="#rating3Dropdown"
                       data-unfold-type="css-animation"
                       data-unfold-duration="300"
                       data-unfold-delay="300"
                       data-unfold-hide-on-scroll="true"
                       data-unfold-animation-in="slideInUp"
                       data-unfold-animation-out="fadeOut">
                        3.9
                    </a>

                    <div id="rating3Dropdown" class="dropdown-menu dropdown-unfold p-3" aria-labelledby="rating3DropdownInvoker" style="width: 190px;">
                        <div class="d-flex align-items-center mb-2">
                            <span class="text-warning mr-2">3.9</span>
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item text-warning">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star-half-alt"></span>
                                    <span class="far fa-star"></span>
                                    <span class="far fa-star"></span>
                                </li>
                            </ul>
                        </div>

                        <p class="text-dark mb-0">Overal Rating</p>
                        <p class="mb-0">Based on 85 reviews</p>
                    </div>
                </div>
                <!-- End Rating -->

                <!-- Bookmark -->
                <div class="ml-auto">
                    <div class="bookmark-checkbox" data-toggle="tooltip" data-placement="top" title="Save">
                        <input type="checkbox" class="bookmark-checkbox-input" id="bookmark4">
                        <label class="bookmark-checkbox-label" for="bookmark4"></label>
                    </div>
                </div>
                <!-- End Bookmark -->
            </div>
            <!-- End Header -->

            <div class="text-center">
                <!-- Avatar -->
                <div class="u-lg-avatar position-relative mx-auto mb-3">
                    <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/100x100/img10.jpg') }}" alt="Image Description">
                    <span class="badge badge-xs badge-outline-success badge-pos badge-pos--bottom-left rounded-circle"></span>
                </div>
                <!-- End Avatar -->

                <!-- Title -->
                <div class="mb-4">
                    <h3 class="h5 mb-0">
                        <a href="employee-profile.html">Ola Nunez</a>
                    </h3>
                    <small class="text-secondary">Fullstack Web developer</small>
                </div>
                <!-- End Title -->

                <p class="mb-0">I am an expert in Web development with 6+ years of experience, currently working as a team leader, solution architect, and developer.</p>
            </div>
        </div>

        <div class="card-footer text-center py-4">
            <!-- Location & Rate -->
            <div class="row align-items-center">
                <div class="col-6 u-ver-divider">
                    <h4 class="small text-secondary mb-0">Location</h4>
                    <img class="img-fluid max-width-3 rounded-circle align-middle mr-1" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/1x1/gb.svg') }}" alt="United Kingdom Flag">
                    <span class="align-middle">Leicester</span>
                </div>
                <div class="col-6">
                    <h4 class="small text-secondary mb-0">Working rate</h4>
                    <span class="align-middle">$49/hr</span>
                </div>
            </div>
            <!-- End Location & Rate -->
        </div>
    </div>
    <!-- End Item -->

    <!-- Item -->
    <div class="card mb-5">
        <div class="card-body p-4">
            <!-- Header -->
            <div class="d-flex align-items-center mb-5">
                <!-- Rating -->
                <div class="position-relative">
                    <a id="rating4DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                       aria-controls="rating4Dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-unfold-event="hover"
                       data-unfold-target="#rating4Dropdown"
                       data-unfold-type="css-animation"
                       data-unfold-duration="300"
                       data-unfold-delay="300"
                       data-unfold-hide-on-scroll="true"
                       data-unfold-animation-in="slideInUp"
                       data-unfold-animation-out="fadeOut">
                        4.4
                    </a>

                    <div id="rating4Dropdown" class="dropdown-menu dropdown-unfold p-3" aria-labelledby="rating4DropdownInvoker" style="width: 190px;">
                        <div class="d-flex align-items-center mb-2">
                            <span class="text-warning mr-2">4.4</span>
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item text-warning">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="far fa-star"></span>
                                </li>
                            </ul>
                        </div>

                        <p class="text-dark mb-0">Overal Rating</p>
                        <p class="mb-0">Based on 5 reviews</p>
                    </div>
                </div>
                <!-- End Rating -->

                <!-- Bookmark -->
                <div class="ml-auto">
                    <div class="bookmark-checkbox" data-toggle="tooltip" data-placement="top" title="Save">
                        <input type="checkbox" class="bookmark-checkbox-input" id="bookmark5">
                        <label class="bookmark-checkbox-label" for="bookmark5"></label>
                    </div>
                </div>
                <!-- End Bookmark -->
            </div>
            <!-- End Header -->

            <div class="text-center">
                <!-- Avatar -->
                <span class="btn btn-lg btn-icon btn-soft-danger rounded-circle mb-3">
                    <span class="btn-icon__inner">BS</span>
                    <span class="badge badge-xs badge-outline-danger badge-pos badge-pos--bottom-left rounded-circle"></span>
                  </span>
                <!-- End Avatar -->

                <!-- Title -->
                <div class="mb-4">
                    <h3 class="h5 mb-0">
                        <a href="employee-profile.html">John Cannon</a>
                    </h3>
                    <small class="text-secondary">Native Mobile and Web developer</small>
                </div>
                <!-- End Title -->

                <p class="mb-0">I'm a senior level mobile/web developer based out of New York, USA. I've been working on web based projects since 2018.</p>
            </div>
        </div>

        <div class="card-footer text-center py-4">
            <!-- Location & Rate -->
            <div class="row align-items-center">
                <div class="col-6 u-ver-divider">
                    <h4 class="small text-secondary mb-0">Location</h4>
                    <img class="img-fluid max-width-3 rounded-circle align-middle mr-1" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/1x1/gb.svg') }}" alt="United Kingdom Flag">
                    <span class="align-middle">Bradford</span>
                </div>
                <div class="col-6">
                    <h4 class="small text-secondary mb-0">Working rate</h4>
                    <span class="align-middle">$25/hr</span>
                </div>
            </div>
            <!-- End Location & Rate -->
        </div>
    </div>
    <!-- End Item -->
</div>

<div class="card-deck d-block d-md-flex">
    <!-- Item -->
    <div class="card mb-5">
        <div class="card-body p-4">
            <!-- Header -->
            <div class="d-flex align-items-center mb-5">
                <!-- Rating -->
                <div class="position-relative">
                    <a id="rating5DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                       aria-controls="rating5Dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-unfold-event="hover"
                       data-unfold-target="#rating5Dropdown"
                       data-unfold-type="css-animation"
                       data-unfold-duration="300"
                       data-unfold-delay="300"
                       data-unfold-hide-on-scroll="true"
                       data-unfold-animation-in="slideInUp"
                       data-unfold-animation-out="fadeOut">
                        4.1
                    </a>

                    <div id="rating5Dropdown" class="dropdown-menu dropdown-unfold p-3" aria-labelledby="rating5DropdownInvoker" style="width: 190px;">
                        <div class="d-flex align-items-center mb-2">
                            <span class="text-warning mr-2">4.1</span>
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item text-warning">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="far fa-star"></span>
                                </li>
                            </ul>
                        </div>

                        <p class="text-dark mb-0">Overal Rating</p>
                        <p class="mb-0">Based on 131 reviews</p>
                    </div>
                </div>
                <!-- End Rating -->

                <!-- Bookmark -->
                <div class="ml-auto">
                    <div class="bookmark-checkbox" data-toggle="tooltip" data-placement="top" title="Save">
                        <input type="checkbox" class="bookmark-checkbox-input" id="bookmark6">
                        <label class="bookmark-checkbox-label" for="bookmark6"></label>
                    </div>
                </div>
                <!-- End Bookmark -->
            </div>
            <!-- End Header -->

            <div class="text-center">
                <!-- Avatar -->
                <span class="btn btn-lg btn-icon btn-soft-success rounded-circle mb-3">
                    <span class="btn-icon__inner">BS</span>
                    <span class="badge badge-xs badge-outline-success badge-pos badge-pos--bottom-left rounded-circle"></span>
                  </span>
                <!-- End Avatar -->

                <!-- Title -->
                <div class="mb-4">
                    <h3 class="h5 mb-0">
                        <a href="employee-profile.html">Leo Snyder</a>
                    </h3>
                    <small class="text-secondary">UK based Web developer</small>
                </div>
                <!-- End Title -->

                <p class="mb-0">I'm an analytical person and always identify business requirements and needs. Design develop and implement solutions will be my goal.</p>
            </div>
        </div>

        <div class="card-footer text-center py-4">
            <!-- Location & Rate -->
            <div class="row align-items-center">
                <div class="col-6 u-ver-divider">
                    <h4 class="small text-secondary mb-0">Location</h4>
                    <img class="img-fluid max-width-3 rounded-circle align-middle mr-1" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/1x1/gb.svg') }}" alt="United Kingdom Flag">
                    <span class="align-middle">Oxford</span>
                </div>
                <div class="col-6">
                    <h4 class="small text-secondary mb-0">Working rate</h4>
                    <span class="align-middle">$110/hr</span>
                </div>
            </div>
            <!-- End Location & Rate -->
        </div>
    </div>
    <!-- End Item -->

    <!-- Item -->
    <div class="card mb-5">
        <div class="card-body p-4">
            <!-- Header -->
            <div class="d-flex align-items-center mb-5">
                <!-- Rating -->
                <div class="position-relative">
                    <a id="rating6DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                       aria-controls="rating6Dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-unfold-event="hover"
                       data-unfold-target="#rating6Dropdown"
                       data-unfold-type="css-animation"
                       data-unfold-duration="300"
                       data-unfold-delay="300"
                       data-unfold-hide-on-scroll="true"
                       data-unfold-animation-in="slideInUp"
                       data-unfold-animation-out="fadeOut">
                        5.0
                    </a>

                    <div id="rating6Dropdown" class="dropdown-menu dropdown-unfold p-3" aria-labelledby="rating6DropdownInvoker" style="width: 190px;">
                        <div class="d-flex align-items-center mb-2">
                            <span class="text-warning mr-2">5.0</span>
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item text-warning">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                </li>
                            </ul>
                        </div>

                        <p class="text-dark mb-0">Overal Rating</p>
                        <p class="mb-0">Based on 1809 reviews</p>
                    </div>
                </div>
                <!-- End Rating -->

                <!-- Bookmark -->
                <div class="ml-auto">
                    <div class="bookmark-checkbox" data-toggle="tooltip" data-placement="top" title="Save">
                        <input type="checkbox" class="bookmark-checkbox-input" id="bookmark7">
                        <label class="bookmark-checkbox-label" for="bookmark7"></label>
                    </div>
                </div>
                <!-- End Bookmark -->
            </div>
            <!-- End Header -->

            <div class="text-center">
                <!-- Avatar -->
                <div class="u-lg-avatar position-relative mx-auto mb-3">
                    <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/100x100/img8.jpg') }}" alt="Image Description">
                    <span class="badge badge-xs badge-outline-success badge-pos badge-pos--bottom-left rounded-circle"></span>
                </div>
                <!-- End Avatar -->

                <!-- Title -->
                <div class="mb-4">
                    <h3 class="h5 mb-0">
                        <a href="employee-profile.html">Chester Haynes</a>
                    </h3>
                    <small class="text-secondary">Web developer</small>
                </div>
                <!-- End Title -->

                <p class="mb-0">Web developer from Leicester and with passion to Web. Contact me if you are looking to build something amazing.</p>
            </div>
        </div>

        <div class="card-footer text-center py-4">
            <!-- Location & Rate -->
            <div class="row align-items-center">
                <div class="col-6 u-ver-divider">
                    <h4 class="small text-secondary mb-0">Location</h4>
                    <img class="img-fluid max-width-3 rounded-circle align-middle mr-1" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/1x1/gb.svg') }}" alt="United Kingdom Flag">
                    <span class="align-middle">Leicester</span>
                </div>
                <div class="col-6">
                    <h4 class="small text-secondary mb-0">Working rate</h4>
                    <span class="align-middle">$65/hr</span>
                </div>
            </div>
            <!-- End Location & Rate -->
        </div>
    </div>
    <!-- End Item -->
</div>

<div class="card-deck d-block d-md-flex">
    <!-- Item -->
    <div class="card mb-5">
        <div class="card-body p-4">
            <!-- Header -->
            <div class="d-flex align-items-center mb-5">
                <!-- Rating -->
                <div class="position-relative">
                    <a id="rating7DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                       aria-controls="rating7Dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-unfold-event="hover"
                       data-unfold-target="#rating7Dropdown"
                       data-unfold-type="css-animation"
                       data-unfold-duration="300"
                       data-unfold-delay="300"
                       data-unfold-hide-on-scroll="true"
                       data-unfold-animation-in="slideInUp"
                       data-unfold-animation-out="fadeOut">
                        4.5
                    </a>

                    <div id="rating7Dropdown" class="dropdown-menu dropdown-unfold p-3" aria-labelledby="rating7DropdownInvoker" style="width: 190px;">
                        <div class="d-flex align-items-center mb-2">
                            <span class="text-warning mr-2">4.5</span>
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item text-warning">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star-half-alt"></span>
                                </li>
                            </ul>
                        </div>

                        <p class="text-dark mb-0">Overal Rating</p>
                        <p class="mb-0">Based on 427 reviews</p>
                    </div>
                </div>
                <!-- End Rating -->

                <!-- Bookmark -->
                <div class="ml-auto">
                    <div class="bookmark-checkbox" data-toggle="tooltip" data-placement="top" title="Save">
                        <input type="checkbox" class="bookmark-checkbox-input" id="bookmark8">
                        <label class="bookmark-checkbox-label" for="bookmark8"></label>
                    </div>
                </div>
                <!-- End Bookmark -->
            </div>
            <!-- End Header -->

            <div class="text-center">
                <!-- Avatar -->
                <div class="u-lg-avatar position-relative mx-auto mb-3">
                    <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/100x100/img12.jpg') }}" alt="Image Description">
                    <span class="badge badge-xs badge-outline-success badge-pos badge-pos--bottom-left rounded-circle"></span>
                </div>
                <!-- End Avatar -->

                <!-- Title -->
                <div class="mb-4">
                    <h3 class="h5 mb-0">
                        <a href="employee-profile.html">James Wade</a>
                    </h3>
                    <small class="text-secondary">PHP and Web developer</small>
                </div>
                <!-- End Title -->

                <p class="mb-0">Self-motivated, efficient and hardworking Web Developer with keen interest in new technical challenges.</p>
            </div>
        </div>

        <div class="card-footer text-center py-4">
            <!-- Location & Rate -->
            <div class="row align-items-center">
                <div class="col-6 u-ver-divider">
                    <h4 class="small text-secondary mb-0">Location</h4>
                    <img class="img-fluid max-width-3 rounded-circle align-middle mr-1" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/1x1/gb.svg') }}" alt="United Kingdom Flag">
                    <span class="align-middle">Leicester</span>
                </div>
                <div class="col-6">
                    <h4 class="small text-secondary mb-0">Working rate</h4>
                    <span class="align-middle">$77/hr</span>
                </div>
            </div>
            <!-- End Location & Rate -->
        </div>
    </div>
    <!-- End Item -->

    <!-- Item -->
    <div class="card mb-5">
        <div class="card-body p-4">
            <!-- Header -->
            <div class="d-flex align-items-center mb-5">
                <!-- Rating -->
                <div class="position-relative">
                    <a id="rating8DropdownInvoker" class="btn btn-xs btn-soft-warning btn-pill" href="javascript:;" role="button"
                       aria-controls="rating8Dropdown"
                       aria-haspopup="true"
                       aria-expanded="false"
                       data-unfold-event="hover"
                       data-unfold-target="#rating8Dropdown"
                       data-unfold-type="css-animation"
                       data-unfold-duration="300"
                       data-unfold-delay="300"
                       data-unfold-hide-on-scroll="true"
                       data-unfold-animation-in="slideInUp"
                       data-unfold-animation-out="fadeOut">
                        3.9
                    </a>

                    <div id="rating8Dropdown" class="dropdown-menu dropdown-unfold p-3" aria-labelledby="rating8DropdownInvoker" style="width: 190px;">
                        <div class="d-flex align-items-center mb-2">
                            <span class="text-warning mr-2">3.9</span>
                            <ul class="list-inline mb-0">
                                <li class="list-inline-item text-warning">
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star"></span>
                                    <span class="fas fa-star-half-alt"></span>
                                    <span class="far fa-star"></span>
                                    <span class="far fa-star"></span>
                                </li>
                            </ul>
                        </div>

                        <p class="text-dark mb-0">Overal Rating</p>
                        <p class="mb-0">Based on 11 reviews</p>
                    </div>
                </div>
                <!-- End Rating -->

                <!-- Bookmark -->
                <div class="ml-auto">
                    <div class="bookmark-checkbox" data-toggle="tooltip" data-placement="top" title="Save">
                        <input type="checkbox" class="bookmark-checkbox-input" id="bookmark9">
                        <label class="bookmark-checkbox-label" for="bookmark9"></label>
                    </div>
                </div>
                <!-- End Bookmark -->
            </div>
            <!-- End Header -->

            <div class="text-center">
                <!-- Avatar -->
                <div class="u-lg-avatar position-relative mx-auto mb-3">
                    <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/100x100/img11.jpg') }}" alt="Image Description">
                    <span class="badge badge-xs badge-outline-danger badge-pos badge-pos--bottom-left rounded-circle"></span>
                </div>
                <!-- End Avatar -->

                <!-- Title -->
                <div class="mb-4">
                    <h3 class="h5 mb-0">
                        <a href="employee-profile.html">Noah Wong</a>
                    </h3>
                    <small class="text-secondary">UI/UX designer &amp; Web developer</small>
                </div>
                <!-- End Title -->

                <p class="mb-0">As a long-experienced developer, I have developed a wide range of a web solutions.</p>
            </div>
        </div>

        <div class="card-footer text-center py-4">
            <!-- Location & Rate -->
            <div class="row align-items-center">
                <div class="col-6 u-ver-divider">
                    <h4 class="small text-secondary mb-0">Location</h4>
                    <img class="img-fluid max-width-3 rounded-circle align-middle mr-1" src="{{ URL::asset('front-v2.5.0/assets/vendor/flag-icon-css/flags/1x1/gb.svg') }}" alt="United Kingdom Flag">
                    <span class="align-middle">London</span>
                </div>
                <div class="col-6">
                    <h4 class="small text-secondary mb-0">Working rate</h4>
                    <span class="align-middle">$55/hr</span>
                </div>
            </div>
            <!-- End Location & Rate -->
        </div>
    </div>
    <!-- End Item -->
</div>

<div class="py-3"></div>

<!-- Pagination -->
<nav aria-label="Page navigation">
    <ul class="pagination justify-content-center mb-0">
        <li class="page-item ml-0">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <li class="page-item active"><a class="page-link" href="#">1</a></li>
        <li class="page-item"><a class="page-link" href="#">2</a></li>
        <li class="page-item disabled"><a class="page-link" href="#">...</a></li>
        <li class="page-item"><a class="page-link" href="#">12</a></li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
    </ul>
</nav>
<!-- End Pagination -->
