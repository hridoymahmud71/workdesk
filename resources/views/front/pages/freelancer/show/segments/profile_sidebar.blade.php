<div class="card shadow-sm p-5 mb-5">
    <div class="border-bottom text-center pb-5 mb-5">
        <!-- Status -->
        <div class="d-flex justify-content-end mb-1">
                <span class="btn btn-xs btn-soft-success btn-pill">
                  <small class="fas fa-circle mr-1"></small>
                  Online
                </span>
        </div>
        <!-- End Status -->

        <!-- Avatar -->
        <div class="mb-3 mx-auto">
            <img class="img-fluid rounded-circle" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img22.jpg') }}" width="110" height="110" alt="Image Description">
        </div>
        <!-- End Avatar -->

        <h1 class="h5">Jorge Fields</h1>

        <!-- Review -->
        <div class="mb-2">
                <span class="text-warning">
                  <small class="fas fa-star"></small>
                  <small class="fas fa-star"></small>
                  <small class="fas fa-star"></small>
                  <small class="fas fa-star"></small>
                  <small class="fas fa-star-half-alt"></small>
                </span>
            <span class="font-weight-semi-bold ml-2">4.7</span>
            <small class="text-muted">(39 reviews)</small>
        </div>
        <!-- End Review -->

        <!-- Additional Info -->
        <ul class="list-inline text-secondary font-size-1 mb-4">
            <li class="list-inline-item">
                <small class="fas fa-map-marker-alt mr-1"></small>
                London, UK
            </li>
            <li class="list-inline-item text-muted">&#8226;</li>
            <li class="list-inline-item">
                Joined Jun 2015
            </li>
        </ul>
        <!-- End Additional Info -->

        <div class="mb-2">
            <a class="btn btn-block btn-sm btn-soft-primary transition-3d-hover" href="#">Hire Me</a>
        </div>

        <a class="text-muted small" href="#">
            <small class="fas fa-flag mr-1"></small>
            Report this user
        </a>
    </div>

    <div class="border-bottom pb-5 mb-5">
        <div id="SVGemployeeStatsIcon" class="svg-preloader row">
            <div class="col-6 mb-5">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-35.svg') }}" alt="SVG"
                             data-parent="#SVGemployeeStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">$35.00</span>
                    <span class="d-block text-secondary font-size-1">Hourly rate</span>
                </div>
                <!-- End Stats -->
            </div>

            <div class="col-6 mb-5">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-37.svg') }}" alt="SVG"
                             data-parent="#SVGemployeeStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">30</span>
                    <span class="d-block text-secondary font-size-1">Jobs done</span>
                </div>
                <!-- End Stats -->
            </div>

            <div class="col-6">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-5.svg') }}" alt="SVG"
                             data-parent="#SVGemployeeStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">85%</span>
                    <span class="d-block text-secondary font-size-1">Success rate</span>
                </div>
                <!-- End Stats -->
            </div>

            <div class="col-6">
                <!-- Stats -->
                <div class="text-center">
                    <figure class="ie-height-48 max-width-5 mb-2 mx-auto">
                        <img class="js-svg-injector" src="{{ URL::asset('front-v2.5.0/assets/svg/icons/icon-7.svg') }}" alt="SVG"
                             data-parent="#SVGemployeeStatsIcon">
                    </figure>
                    <span class="h6 d-block font-weight-medium mb-0">345</span>
                    <span class="d-block text-secondary font-size-1">Repeat clients</span>
                </div>
                <!-- End Stats -->
            </div>
        </div>
    </div>

    <div class="border-bottom pb-5 mb-5">
        <h2 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Bio</h2>

        <p class="font-size-1 mb-0">I love coding and problem solving. I have 3+ years of experience in the web development sector. Love working on both server and client side code. I like to get to know my clients closely to facilitate better communication.</p>
    </div>

    <div class="border-bottom pb-5 mb-5">
        <h3 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Languages</h3>

        <!-- Languages -->
        <span class="d-block font-size-1 font-weight-medium mb-1">English - <span class="text-muted font-weight-normal">Advanced</span></span>
        <span class="d-block font-size-1 font-weight-medium">French - <span class="text-muted font-weight-normal">Unspecified</span></span>
        <!-- End Languages -->
    </div>

    <div class="border-bottom pb-5 mb-5">
        <h3 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Skills</h3>

        <!-- Skills -->
        <a class="btn btn-xs btn-gray mb-1" href="#">Android</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">iOS</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">PHP</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">Developer</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">JavaScript</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">CSS3</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">HTML5</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">Bootstrap</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">Angular</a>
        <a class="btn btn-xs btn-gray mb-1" href="#">React.js</a>
        <!-- End Skills -->
    </div>

    <div class="border-bottom pb-5 mb-5">
        <h3 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Rewards</h3>

        <!-- Rewards -->
        <div class="mb-3">
            <img class="max-width-5 mr-1" src="{{ URL::asset('front-v2.5.0/assets/svg/illustrations/top-level-award.svg') }}" alt="Image Description" title="Top Seller">
            <img class="max-width-5 mx-1" src="{{ URL::asset('front-v2.5.0/assets/svg/illustrations/verified-user.svg') }}" alt="Image Description" title="Verified user">
            <img class="max-width-5 ml-1" src="{{ URL::asset('front-v2.5.0/assets/svg/illustrations/top-endorsed.svg') }}" alt="Image Description" title="Top Endorsed">
        </div>
        <!-- End Rewards -->

        <p class="small mb-0">Industry expertise: IT, Technology and Business</p>
    </div>

    <div class="border-bottom pb-5 mb-5">
        <h3 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Work experience</h3>

        <!-- Work Experience Info -->
        <ul class="list-unstyled u-indicator-vertical-dashed">
            <li class="media u-indicator-vertical-dashed-item">
                  <span class="btn btn-xs btn-icon btn-soft-success rounded-circle mr-3">
                    <span class="btn-icon__inner">S</span>
                  </span>
                <div class="media-body">
                    <h4 class="font-size-1 mb-1">Product Director</h4>
                    <ul class="list-unstyled small text-secondary mb-0">
                        <li>
                            <a href="#">Spotify Inc.</a>
                        </li>
                        <li>Jan 2008 to Mar 2010</li>
                        <li>San Francisco, California</li>
                    </ul>
                </div>
            </li>

            <li class="media u-indicator-vertical-dashed-item">
                  <span class="btn btn-xs btn-icon btn-soft-primary rounded-circle mr-3">
                    <span class="btn-icon__inner">D</span>
                  </span>
                <div class="media-body">
                    <h4 class="font-size-1 mb-1">Channel Sales Director</h4>
                    <ul class="list-unstyled small text-secondary mb-0">
                        <li>
                            <a href="#">Dropbox Inc.</a>
                        </li>
                        <li>Apr 2010 to Sep 2015</li>
                        <li>Newark, NJ</li>
                    </ul>
                </div>
            </li>

            <li class="media u-indicator-vertical-dashed-item">
                  <span class="btn btn-xs btn-icon btn-soft-danger rounded-circle mr-3">
                    <span class="btn-icon__inner">A</span>
                  </span>
                <div class="media-body">
                    <h4 class="font-size-1 mb-1">Business manager</h4>
                    <ul class="list-unstyled small text-secondary mb-0">
                        <li>
                            <a href="#">Airbnb Inc.</a>
                        </li>
                        <li>Oct 2015 to Present</li>
                        <li>San Francisco, California</li>
                    </ul>
                </div>
            </li>
        </ul>
        <!-- End Work Experience Info -->
    </div>

    <h4 class="font-size-1 font-weight-semi-bold text-uppercase mb-3">Linked accounts</h4>

    <!-- Linked Account -->
    <a class="media align-items-center mb-3" href="#">
        <div class="u-sm-avatar mr-3">
            <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img3.jpg') }}" alt="Image Description">
        </div>
        <div class="media-body">
            <h4 class="font-size-1 text-dark mb-0">Slack</h4>
            <small class="d-block text-secondary">jorgefields.slack.com</small>
        </div>
    </a>
    <!-- End Linked Account -->

    <!-- Linked Account -->
    <a class="media align-items-center" href="#">
        <div class="u-sm-avatar mr-3">
            <img class="img-fluid" src="{{ URL::asset('front-v2.5.0/assets/img/160x160/img19.png') }}" alt="Image Description">
        </div>
        <div class="media-body">
            <h4 class="font-size-1 text-dark mb-0">Twitter</h4>
            <small class="d-block text-secondary">twitter.com/jorgefields</small>
        </div>
    </a>
    <!-- End Linked Account -->
</div>
