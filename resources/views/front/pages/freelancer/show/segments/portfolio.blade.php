<div class="mb-9">
    <div class="card-deck card-sm-gutters-2 d-block d-sm-flex">
        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img2.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Atlassian</h2>
                <small class="d-block text-secondary">Website design</small>
            </div>
        </a>
        <!-- End News Blog -->

        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img12.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Asana</h2>
                <small class="d-block text-secondary">Other</small>
            </div>
        </a>
        <!-- End News Blog -->

        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img4.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Slack</h2>
                <small class="d-block text-secondary">Blog</small>
            </div>
        </a>
        <!-- End News Blog -->
    </div>

    <div class="card-deck card-sm-gutters-2 d-block d-sm-flex">
        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img11.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Spotify</h2>
                <small class="d-block text-secondary">Website design</small>
            </div>
        </a>
        <!-- End News Blog -->

        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img10.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Mapbox</h2>
                <small class="d-block text-secondary">Android App</small>
            </div>
        </a>
        <!-- End News Blog -->

        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img13.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Dropbox</h2>
                <small class="d-block text-secondary">Android &amp; iOS Apps</small>
            </div>
        </a>
        <!-- End News Blog -->
    </div>

    <div class="card-deck card-sm-gutters-2 d-block d-sm-flex">
        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img14.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Google</h2>
                <small class="d-block text-secondary">iOS App</small>
            </div>
        </a>
        <!-- End News Blog -->

        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img3.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Envato</h2>
                <small class="d-block text-secondary">Other</small>
            </div>
        </a>
        <!-- End News Blog -->

        <!-- News Blog -->
        <a class="card card-text-dark border-0 shadow-sm transition-3d-hover mb-5" href="#">
            <img class="card-img-top" src="{{ URL::asset('front-v2.5.0/assets/img/380x270/img7.jpg') }}" alt="Image Description">
            <div class="card-body p-4">
                <h2 class="h6 mb-0">Htmlstream</h2>
                <small class="d-block text-secondary">Website design</small>
            </div>
        </a>
        <!-- End News Blog -->
    </div>

    <!-- Pagination -->
    <div class="d-flex justify-content-between align-items-center">
        <small class="d-none d-sm-inline-block text-secondary">Showing 9 out of 30</small>

        <nav aria-label="Page navigation">
            <ul class="pagination justify-content-end">
                <li class="page-item ml-0">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item active"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item disabled"><a class="page-link" href="#">...</a></li>
                <li class="page-item"><a class="page-link" href="#">4</a></li>
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
    <!-- End Pagination -->
</div>
