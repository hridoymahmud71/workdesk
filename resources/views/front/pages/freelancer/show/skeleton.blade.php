@extends('front.master')
@section('content')
    <div class="container space-2">
        <div class="row">
            <div class="col-lg-4 mb-9 mb-lg-0">
                <!-- Sidebar Info -->
                @include('front.pages.freelancer.show.segments.profile_sidebar')
                <!-- End Sidebar Info -->
            </div>

            <div class="col-lg-8">
                <div class="pl-lg-4">
                    <div class="mb-3">
                        <h4 class="h5 mb-0">Portfolio</h4>
                    </div>


                    @include('front.pages.freelancer.show.segments.portfolio')
                    @include('front.pages.freelancer.show.segments.reviews')


                </div>
            </div>
        </div>
    </div>
@endsection
