<!DOCTYPE html>
<html lang="en">
<head>

    @php
        $site_name  = site_name();
        $exp_site_name = explode(" ", $site_name);
        $site_acronym = "";

        foreach ($exp_site_name as $init) {
          $site_acronym .= strtoupper($init[0]).".";
        }
    @endphp
    <!-- Title -->
    <title>{{ $title ?? site_name() }}</title>

    <!-- Required Meta Tags Always Come First -->
    @include('front.partials.meta')
    @include('front.partials.links')

</head>
<body>
<!-- ========== HEADER ========== -->
<header id="header" class="u-header">
    <div class="u-header__section">
        <!-- Topbar -->
        @include('front.partials.topbar')
        <!-- End Topbar -->

        @include('front.partials.logo_and_navbar.skeleton')
    </div>
</header>
<!-- ========== END HEADER ========== -->

<!-- ========== MAIN CONTENT ========== -->
<main id="content" role="main">
@yield('content')
</main>
<!-- ========== END MAIN CONTENT ========== -->

<!-- ========== FOOTER ========== -->
@include('front.partials.footer.skeleton')
<!-- ========== END FOOTER ========== -->

<!-- Go to Top -->
@include('front.partials.go_to_top')
<!-- End Go to Top -->

@include('front.partials.scripts')

<!-- JS Plugins Init. -->
<script>
    $(window).on('load', function () {
        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 767.98,
            hideTimeOut: 0
        });

        // initialization of svg injector module
        $.HSCore.components.HSSVGIngector.init('.js-svg-injector');
    });

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#header'));
        // initialization of unfold component
        $.HSCore.components.HSUnfold.init($('[data-unfold-target]'));
        // initialization of forms
        $.HSCore.components.HSFocusState.init();
        // initialization of form validation
        $.HSCore.components.HSValidation.init('.js-validate');
        // initialization of fancybox
        $.HSCore.components.HSFancyBox.init('.js-fancybox');
        // initialization of slick carousel
        $.HSCore.components.HSSlickCarousel.init('.js-slick-carousel');
        // initialization of select picker
        $.HSCore.components.HSSelectPicker.init('.js-select');
        // initialization of forms
        $.HSCore.components.HSRangeSlider.init('.js-range-slider');
        // initialization of show animations
        $.HSCore.components.HSShowAnimation.init('.js-animation-link');
        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');


    });
    
    function elem_exists(elem) {
        return $(elem).length;
    }
</script>

@yield('in_blade_js_codes')
</body>
</html>
