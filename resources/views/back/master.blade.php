<!DOCTYPE html>
<html lang="en">
<head>
    <title>{{ $title ?? site_name() }}</title>
    @include('back.partials.meta')
    @include('back.partials.links')
</head>

<body>

<!-- Begin page -->
<div class="wrapper">

    <!-- ========== Left Sidebar Start ========== -->
    @include('back.partials.left_side_menu.skeleton')
    <!-- Left Sidebar End -->

    <!-- ============================================================== -->
    <!-- Start Page Content here -->
    <!-- ============================================================== -->

    <div class="content-page">
        <div class="content">

            <!-- Topbar Start -->
            @include('back.partials.topbar.skeleton')
            <!-- end Topbar -->

            <!-- Start Content-->
            <div class="container-fluid">

                <!-- start page title -->
                @include('back.partials.title_and_breadcrurmb')
                <!-- end page title -->

                @yield('content')

            </div> <!-- container -->

        </div> <!-- content -->

        <!-- Footer Start -->
        @include('back.partials.footer')
        <!-- end Footer -->

    </div>

    <!-- ============================================================== -->
    <!-- End Page content -->
    <!-- ============================================================== -->


</div>
<!-- END wrapper -->


<!-- Right Sidebar -->
@include('back.partials.right_bar')
<!-- /Right-bar -->


@include('back.partials.scripts')
</body>
</html>
