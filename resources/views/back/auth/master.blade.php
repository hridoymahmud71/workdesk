<!DOCTYPE html>
<html lang="en">

<head>
    <title>{{ $title ?? site_name() }}</title>
    @include('back.partials.meta')
    @include('back.partials.links')

</head>

<body class="auth-fluid-pages pb-0">

<div class="auth-fluid">
    <!--Auth fluid left content -->
    <div class="auth-fluid-form-box">
        <div class="align-items-center d-flex h-100">
            <div class="card-body">

                <!-- Logo -->
                @include('back.auth.partials.logo')

                <!-- title-->
                @include('back.auth.partials.title_and_description')

                @yield('form_content')

                @yield('footer_content')



            </div> <!-- end .card-body -->
        </div> <!-- end .align-items-center.d-flex.h-100-->
    </div>
    <!-- end auth-fluid-form-box-->

    <!-- Auth fluid right content -->
    @include('back.auth.partials.right_content')
    <!-- end Auth fluid right content -->
</div>
<!-- end auth-fluid-->

<!-- App js -->
@include('back.partials.scripts')
</body>

</html>
