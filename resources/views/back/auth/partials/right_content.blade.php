<div class="auth-fluid-right text-center">
    <div class="auth-user-testimonial">

        @if(isset($auth_right_content_html))
            {{$auth_right_content_html}}
        @else
            <h2 class="mb-3">{{site_name()}}</h2>
            <p class="lead"><i class="mdi mdi-format-quote-open"></i> {{ site_description()  }}<i class="mdi mdi-format-quote-close"></i>
            </p>
            <p>
            version {{application_version()}}
            </p>
        @endif
    </div> <!-- end auth-user-testimonial-->
</div>
