@extends('back.auth.master')

@section('form_content')
    <!-- form -->
    <!-- email send icon with text-->
    <div class="text-center m-auto">
        <img src="{{ URL::asset('Hyper_v1.5.0/dist/assets/images/mail_sent.svg') }}" alt="mail sent image" height="64" />
        <h4 class="text-dark-50 text-center mt-4 font-weight-bold">{{__('Please check your email')}}</h4>
        <p class="text-muted mb-4">
            {{__('Email is sent.Please check your inbox')}}
        </p>
    </div>

    <!-- form -->
    <form action="index.html">
        <div class="form-group mb-0 text-center">
            <a href="{{route('admin_login')}}" class="btn btn-primary btn-block"><i class="mdi mdi-home mr-1"></i> {{__('Log In')}}</a>
        </div>
    </form>
    <!-- end form-->
@endsection


@section('footer_content')
    <!-- Footer-->
    <footer class="footer footer-alt">
    </footer>
@endsection
