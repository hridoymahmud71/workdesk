@extends('back.auth.master')

@section('form_content')
    <!-- form -->
    <form action="#">
        <div class="form-group">
            <label for="emailaddress">{{__('Email Address')}}</label>
            <input class="form-control" type="email" id="emailaddress" required="" placeholder="Enter your email">
        </div>
        <div class="form-group mb-0 text-center">
            <button class="btn btn-primary btn-block" type="submit"><i class="mdi mdi-lock-reset"></i> {{__('Reset Password')}}</button>
        </div>
    </form>
    <!-- end form-->
@endsection


@section('footer_content')
    <!-- Footer-->
    <footer class="footer footer-alt">
        <p class="text-muted"><a href="{{ route('admin_login') }}" class="text-muted ml-1"><b>{{ __('Log In')}}</b></a></p>
    </footer>
@endsection
