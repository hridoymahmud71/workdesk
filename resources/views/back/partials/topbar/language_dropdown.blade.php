<li class="dropdown notification-list topbar-dropdown">
    <a class="nav-link dropdown-toggle arrow-none" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
        <img src="{{ URL::asset('Hyper_v1.5.0/dist/assets/images/flags/us.jpg') }}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">English</span> <i class="mdi mdi-chevron-down"></i>
    </a>
    <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated topbar-dropdown-menu">

        <!-- item-->
        <a href="javascript:void(0);" class="dropdown-item notify-item">
            <img src="{{ URL::asset('Hyper_v1.5.0/dist/assets/images/flags/germany.jpg') }}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">German</span>
        </a>

        <!-- item-->
        <a href="javascript:void(0);" class="dropdown-item notify-item">
            <img src="{{ URL::asset('Hyper_v1.5.0/dist/assets/images/flags/italy.jpg') }}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Italian</span>
        </a>

        <!-- item-->
        <a href="javascript:void(0);" class="dropdown-item notify-item">
            <img src="{{ URL::asset('Hyper_v1.5.0/dist/assets/images/flags/spain.jpg') }}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Spanish</span>
        </a>

        <!-- item-->
        <a href="javascript:void(0);" class="dropdown-item notify-item">
            <img src="{{ URL::asset('Hyper_v1.5.0/dist/assets/images/flags/russia.jpg') }}" alt="user-image" class="mr-1" height="12"> <span class="align-middle">Russian</span>
        </a>

    </div>
</li>
