<div class="navbar-custom">
    <ul class="list-unstyled topbar-right-menu float-right mb-0">

        @include('back.partials.topbar.language_dropdown')

        @include('back.partials.topbar.notification_dropdown')

        @include('back.partials.topbar.profile_dropdown')

    </ul>
    <button class="button-menu-mobile open-left disable-btn">
        <i class="mdi mdi-menu"></i>
    </button>
    <div class="app-search">
        <form>
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="mdi mdi-magnify"></span>
                <div class="input-group-append">
                    <button class="btn btn-primary" type="submit">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
