<!-- App favicon -->
<link rel="shortcut icon" href="{{ URL::asset('Hyper_v1.5.0/dist/assets/images/favicon.ico') }}">

<!-- App css -->
<link href="{{ URL::asset('Hyper_v1.5.0/dist/assets/css/icons.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ URL::asset('Hyper_v1.5.0/dist/assets/css/app.min.css') }}" rel="stylesheet" type="text/css" />
