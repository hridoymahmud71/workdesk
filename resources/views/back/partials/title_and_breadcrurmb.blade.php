@php
use Illuminate\Support\Arr; //for array helper
@endphp

<div class="row">
    <div class="col-12">
        <div class="page-title-box">
            <div class="page-title-right">
                @php
                $breadcrumb = isset($breadcrumb) && is_array($breadcrumb) ? $breadcrumb : array();

                $first_crumb = Arr::first($breadcrumb, function ($value, $key) {
                    return $value;
                });

                $last_crumb = Arr::last($breadcrumb, function ($value, $key) {
                    return $value;
                });
                @endphp
                <ol class="breadcrumb m-0">
                    @foreach($breadcrumb as $key_of_crumb => $a_crumb)
                        @if($a_crumb != $last_crumb )
                            <li class="breadcrumb-item">
                                @if($a_crumb == $first_crumb)
                                    <a href="{{ isset($a_crumb['anchor']) && !empty($a_crumb['anchor']) ? $a_crumb['anchor']:route("admin_default")}}">
                                        {{ isset($a_crumb['name']) && !empty($a_crumb['name']) ? $a_crumb['name']:__("Admin")}}
                                    </a>
                                @else
                                    <a href="{{ isset($a_crumb['anchor']) && !empty($a_crumb['anchor']) ? $a_crumb['anchor']:"javascript: void(0);"}}">
                                        {{ isset($a_crumb['name']) && !empty($a_crumb['name']) ? $a_crumb['name']:""}}
                                    </a>
                                @endif
                            </li>
                        @else
                            {{-- last crumb --}}
                            <li class="breadcrumb-item active">
                                {{ isset($a_crumb['name']) && !empty($a_crumb['name']) ? $a_crumb['name']:""}}
                            </li>
                        @endif
                    @endforeach
                </ol>
            </div>
            <h4 class="page-title">{{$page_name??""}}</h4>
        </div>
    </div>
</div>
