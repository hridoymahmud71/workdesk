<div class="left-side-menu">

    <div class="slimscroll-menu" id="left-side-menu-container">

        <!-- LOGO -->
        @include('back.partials.left_side_menu.logo')

        <!--- Sidemenu -->
        @include('back.partials.left_side_menu.side_menu')

        <!-- Help Box -->
         @include('back.partials.left_side_menu.help')
        <!-- end Help Box -->
        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>
    <!-- Sidebar -left -->

</div>
